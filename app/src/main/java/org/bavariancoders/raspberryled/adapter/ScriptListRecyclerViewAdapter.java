/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.adapter;


import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.bavariancoders.raspberryled.model.Container;
import org.bavariancoders.raspberryled.model.ScriptContainer;
import org.bavariancoders.raspberryled.model.ScriptListContainer;
import org.bavariancoders.raspberryled.R;

/**
 * Created by BavarianCoders on 21.08.17
 */

public class ScriptListRecyclerViewAdapter extends RecyclerView.Adapter<ScriptListRecyclerViewAdapter.MyViewHolder> {
    private Container<ScriptContainer> scriptContainer = ScriptListContainer.instance();
    private static ItemClickListener itemClickListener;
    private static ButtonClickListener deleteButtonClickListener, loadButtonClickListener;

    public ScriptListRecyclerViewAdapter () {
        super();
    }

    public void setOnItemClickListener (ItemClickListener itemClickListener) {
        ScriptListRecyclerViewAdapter.itemClickListener = itemClickListener;
    }

    public void setOnDeleteButtonClickListener (ButtonClickListener deleteButtonClickListener) {
        ScriptListRecyclerViewAdapter.deleteButtonClickListener = deleteButtonClickListener;
    }

    public void setOnLoadButtonClickListener (ButtonClickListener loadButtonClickListener) {
        ScriptListRecyclerViewAdapter.loadButtonClickListener = loadButtonClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.script_loader_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.name.setText(scriptContainer.getLinkElement(position).getName());
        if (position == 0){
            holder.name.setBackgroundColor(Color.rgb(0, 255, 0));
            holder.name.setText("Add new Script +");
            holder.delete.setVisibility(View.INVISIBLE);
            holder.load.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return scriptContainer.getSize();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{
        private TextView name;
        private Button load, delete;

        private MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.script_row_name);
            load = (Button) view.findViewById(R.id.script_loader_load);
            delete = (Button) view.findViewById(R.id.script_loader_delete);
            name.setOnClickListener(this);

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteButtonClickListener.onButtonClick(getAdapterPosition(), v);
                }
            });

            load.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loadButtonClickListener.onButtonClick(getAdapterPosition(), v);
                }
            });

        }

        @Override
        public void onClick(View v) {
            itemClickListener.onItemClick(getAdapterPosition(), v);
        }

        @Override
        public boolean onLongClick(View v){
            /* TODO */
            return false;
        }
    }
}
