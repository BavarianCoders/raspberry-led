/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.bavariancoders.raspberryled.model.Container;
import org.bavariancoders.raspberryled.model.ScriptContainer;
import org.bavariancoders.raspberryled.R;

import java.util.Collections;

/**
 * Created by BavarianCoders on 23.03.17
 */

public class ScriptRecyclerViewAdapter extends RecyclerView.Adapter<ScriptRecyclerViewAdapter.ViewHolder> implements ItemTouchHelperAdapter{
    private static OnStartDragListener dragListener;
    private ScriptContainer mDataset = null;
    private static ItemClickListener clickListener;
    private static ButtonClickListener deleteButtonListener;
    private static ButtonClickListener moveButtonListener;

    public ScriptRecyclerViewAdapter (Container container) {
        mDataset = (ScriptContainer) container;
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(mDataset.getScriptList(), i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(mDataset.getScriptList(), i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public void onItemDismiss(int position) {
        mDataset.remove(mDataset.getLinkElement(position));
        notifyItemRemoved(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
            View.OnLongClickListener {

        private TextView kind, param;
        private Button move, delete;
        private ImageView dragHandle;

        public ViewHolder(View view) {
            super(view);

            kind = (TextView) view.findViewById(R.id.skript_row_kind);
            param = (TextView) view.findViewById(R.id.skript_row_param);
            delete = (Button) view.findViewById(R.id.skript_elem_delet);
            move = (Button) view.findViewById(R.id.skript_elem_swap);
            dragHandle = (ImageView) view.findViewById(R.id.drag_handle);

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteButtonListener.onButtonClick(getAdapterPosition(), v);
                }
            });

            move.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    moveButtonListener.onButtonClick(getAdapterPosition(), v);
                }
            });

            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }

        @Override
        public boolean onLongClick(View v) {
            // clickListener.onItemLongClick(getAdapterPosition(), v);
            return false;
        }
    }

    public void setOnClickListener (ItemClickListener clickListener) {
        ScriptRecyclerViewAdapter.clickListener = clickListener;
    }

    public void setOnDeleteButtonListener (ButtonClickListener listener) {
        ScriptRecyclerViewAdapter.deleteButtonListener = listener;
    }

    public void setOnMoveButtonListener (ButtonClickListener listener) {
        ScriptRecyclerViewAdapter.moveButtonListener = listener;
    }

    public void setOnStartDragListener (OnStartDragListener dragListener) {
        ScriptRecyclerViewAdapter.dragListener = dragListener;
    }

    @Override
    public ScriptRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.skript_row, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.param.setText(mDataset.getLinkElement(position).paramtoText());
        holder.kind.setText(mDataset.getLinkElement(position).kindtoText());

        if (mDataset.getLinkElement(position).getSwapSet()) {
            holder.param.setBackgroundColor(Color.rgb(80, 80, 80));
            holder.kind.setBackgroundColor(Color.rgb(80, 80, 80));
        }else{
            holder.param.setBackgroundColor(Color.rgb(117,117,117));
            holder.kind.setBackgroundColor(Color.rgb(117,117,117));
        }

        holder.dragHandle.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                dragListener.onStartDrag(holder);
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface ScriptItemClickListener {}
}
