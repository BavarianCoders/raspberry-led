/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.bavariancoders.raspberryled.model.Container;
import org.bavariancoders.raspberryled.model.Device;
import org.bavariancoders.raspberryled.model.DeviceContainer;
import org.bavariancoders.raspberryled.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BavarianCoders on 04.03.17
 */

public class DeviceRecyclerViewAdapter extends RecyclerView.Adapter<DeviceRecyclerViewAdapter.ViewHolder>{
    private DeviceContainer mDataset = null;
    private static ItemClickListener clickListener;
    private static ButtonClickListener chooseColorListener;
    private SparseBooleanArray selectedItems = new SparseBooleanArray();
    private Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
            View.OnLongClickListener{
        // each data item is just a string in this case
        private TextView ipPortView, nameView;
        private FloatingActionButton chooseColor;
        private ImageView isUp;
        private ViewHolder(View view) {
            super(view);
            ipPortView = (TextView) view.findViewById(R.id.txt_data);
            nameView = (TextView) view.findViewById(R.id.device_name);
            chooseColor = (FloatingActionButton) view.findViewById(R.id.choose_color);
            isUp = (ImageView) view.findViewById(R.id.is_up);

            chooseColor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    chooseColorListener.onButtonClick(getAdapterPosition(), v);
                }
            });

            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(getAdapterPosition(), v);
            return false;
        }
    }

    public void setOnItemClickListener(ItemClickListener clickListener) {
        DeviceRecyclerViewAdapter.clickListener = clickListener;
    }

    public void setOnChooseColorClickListener (ButtonClickListener listener) {
        DeviceRecyclerViewAdapter.chooseColorListener = listener;
    }

    public DeviceRecyclerViewAdapter(Container container, Context context) {
        this.mDataset = (DeviceContainer) container;
        this.context = context;
    }

    @Override
    public DeviceRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DeviceRecyclerViewAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.deviceitem, parent, false));
    }

    @Override
    public void onBindViewHolder(DeviceRecyclerViewAdapter.ViewHolder holder, int position) {
        Device device = mDataset.getLinkElement(position);
        holder.ipPortView.setText(device.getIP() + ":" + device.getPort());
        holder.nameView.setText(device.getName());
        holder.chooseColor.setBackgroundTintList(ColorStateList.valueOf(device.getLastColor()));
        holder.itemView.setActivated(selectedItems.get(position, false));
        holder.itemView.setSelected(selectedItems.get(position, false));
        holder.isUp.setColorFilter(context.getResources().getColor(R.color.material_green));
        holder.isUp.setRotation(0f);
        if (!device.getIsReachable()) {
            holder.isUp.setRotation(180f);
            holder.isUp.setColorFilter(context.getResources().getColor(R.color.material_red));
        }
    }

    public void toggleSelection (int position) {
        if (selectedItems.get(position, false))
            selectedItems.delete(position);
        else
            selectedItems.put(position, true);
        notifyItemChanged(position);
    }

    public void clearSelection () {
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount () {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList<Integer>();
        for (int i = 0; i < getSelectedItemCount(); i++)
            items.add(selectedItems.keyAt(i));

        return items;
    }

    @Override
    public int getItemCount() {
        return mDataset.getSize();
    }

}
