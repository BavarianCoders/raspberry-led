/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.net;

import org.bavariancoders.raspberryled.ConstantsFunctions;
import org.bavariancoders.raspberryled.model.Device;
import org.bavariancoders.raspberryled.net.interfaces.OnProgressUpdateListener;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by BavarianCoders on 01.10.17
 */

public class ServerOnlineTask {
    private Device[] devices;
    private OnProgressUpdateListener progressUpdateListener;

    public ServerOnlineTask (Device... devices) {
        this.devices = devices;
    }

    public ServerOnlineTask (OnProgressUpdateListener progressUpdateListener, Device... devices) {
        this.devices = devices;
        this.progressUpdateListener = progressUpdateListener;
    }

    public List<Future<Object>> start (int numberOfThreads) {
        ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
        List<Future<Object>> returnList = new ArrayList<>();
        for (int i = 0; i < numberOfThreads; i++) {
            Callable<Object> callable = new ServerOnlineCallable(devices[i]);
            returnList.add(executor.submit(callable));
        }
        executor.shutdown();
        return returnList;
    }

    public void setOnProgressUpdateListener (OnProgressUpdateListener progressUpdateListener) {
        this.progressUpdateListener = progressUpdateListener;
    }

    private class ServerOnlineCallable implements Callable<Object> {
        private Device device;

        ServerOnlineCallable(Device device) {
            this.device = device;
        }

        @Override
        public Object call() {
            InetAddress serverAddr;
            Socket socket;
            try {
                serverAddr = InetAddress.getByName(device.getIP());
                if (!serverAddr.isReachable(ConstantsFunctions.TIMEOUT)) {
                    device.setIsReachable(false);
                    if (progressUpdateListener != null) progressUpdateListener.onProgressUpdate(device);
                    return null;
                }

                socket = new Socket();
                socket.connect(new InetSocketAddress(device.getIP(),
                        device.getPort()), ConstantsFunctions.TIMEOUT);
                device.setSocket(socket);
                device.setIsReachable(true);
                if (progressUpdateListener != null) progressUpdateListener.onProgressUpdate(device);
                return null;
            } catch (IOException e) {
                // ignore
            }
            device.setIsReachable(false);
            device.setSocket(null);
            if(progressUpdateListener != null) progressUpdateListener.onProgressUpdate(device);

            return null;
        }
    }

}
