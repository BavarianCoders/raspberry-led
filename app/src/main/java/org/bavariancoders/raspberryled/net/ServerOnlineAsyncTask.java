/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.net;

import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import org.bavariancoders.raspberryled.ConstantsFunctions;
import org.bavariancoders.raspberryled.model.Device;
import org.bavariancoders.raspberryled.net.interfaces.OnProgressUpdateListener;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Created by BavarianCoders on 28.07.17
 */

public class ServerOnlineAsyncTask extends AsyncTask<Device, Device, Void> {

    private SwipeRefreshLayout swiperefresh;
    private AppCompatActivity activity;
    private OnProgressUpdateListener progressUpdateListener;

    public ServerOnlineAsyncTask(AppCompatActivity activity, SwipeRefreshLayout swiperefresh){
        this.swiperefresh = swiperefresh;
        this.activity = activity;
    }

    public ServerOnlineAsyncTask(AppCompatActivity activity) {
        this.activity = activity;
    }

    public ServerOnlineAsyncTask(AppCompatActivity activity,
                                 SwipeRefreshLayout swiperefresh,
                                 OnProgressUpdateListener progressUpdateListener) {
        this.progressUpdateListener = progressUpdateListener;
        this.activity = activity;
        this.swiperefresh = swiperefresh;
    }

    public ServerOnlineAsyncTask(AppCompatActivity activity, OnProgressUpdateListener progressUpdateListener) {
        this.activity = activity;
        this.progressUpdateListener = progressUpdateListener;
    }

    @Override
    protected Void doInBackground(Device... params) {
        InetAddress serverAddr;
        Socket socket;

        for (int i = 0; i < params.length; i++) {
            try {
                serverAddr = InetAddress.getByName(params[i].getIP());
                if (!serverAddr.isReachable(ConstantsFunctions.TIMEOUT)) {
                    params[i].setIsReachable(false);
                    publishProgress(params[i]);
                    continue;
                }

                socket = new Socket();
                socket.connect(new InetSocketAddress(params[i].getIP(),
                            params[i].getPort()), ConstantsFunctions.TIMEOUT);
                params[i].setSocket(socket);
                params[i].setIsReachable(true);
                publishProgress(params[i]);
                continue;
            } catch (IOException e) {
                // ignore
            }
            params[i].setIsReachable(false);
            params[i].setSocket(null);
            publishProgress(params[i]);
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Device... params) {
        if (progressUpdateListener != null) progressUpdateListener.onProgressUpdate(params);
        else {
            if (params[0].getIsReachable())
                Toast.makeText(activity, "Connection established to " + params[0].toString(), Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(activity, "Connection time out " + params[0].toString(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPostExecute(Void result) {
        if (swiperefresh != null && swiperefresh.isRefreshing())
            swiperefresh.setRefreshing(false);
    }

    @Override
    protected void onPreExecute() {
        if (swiperefresh != null && !swiperefresh.isRefreshing())
            swiperefresh.setRefreshing(true);
    }

    public void setOnProgressUpdateListener (OnProgressUpdateListener progressUpdateListener) {
        this.progressUpdateListener = progressUpdateListener;
    }
}
