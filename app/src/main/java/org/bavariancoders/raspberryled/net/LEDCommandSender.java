/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.net;

import org.bavariancoders.raspberryled.ConstantsFunctions;
import org.bavariancoders.raspberryled.model.Device;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by BavarianCoders on 28.07.17
 */

public class LEDCommandSender {
    private String command = null;
    private Device[] devices;
    public LEDCommandSender(String command, Device... devices){
        this.command = command;
        this.devices = devices;
    }

    public List<Future<Object>> start (int numberOfThreads) {
        ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
        List<Future<Object>> returnList = new ArrayList<Future<Object>>();
        for (int i = 0 ; i < numberOfThreads; i++) {
            Callable<Object> callable = new SendLEDCommandCallable(devices[i]);
            returnList.add(executor.submit(callable));
        }

        return returnList;
    }

   private class SendLEDCommandCallable implements Callable<Object> {

       private Device device;

       SendLEDCommandCallable (Device device) {
           this.device = device;
       }

       @Override
       public Object call() throws IOException{

           PrintWriter writer;

           if (!device.getIsReachable())
               throw new IOException("Failed to send LED command. Try reconnect: " + device.toString());
           writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(device.getSocket().getOutputStream())
           ), true);
           if (command.compareTo("turnOn") == 0)
               writer.println(ConstantsFunctions.getFormatedRGB(device.getLastColor()));
           else if (command.compareTo("turnOff") == 0)
               writer.println("000000000");
           else
               writer.println(command);

           return null;
       }
   }
}
