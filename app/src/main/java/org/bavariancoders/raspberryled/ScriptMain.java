/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Toast;
import android.content.Intent;


import org.bavariancoders.raspberryled.adapter.ItemClickListener;
import org.bavariancoders.raspberryled.adapter.ItemTouchHelperCallback;
import org.bavariancoders.raspberryled.adapter.OnStartDragListener;
import org.bavariancoders.raspberryled.dialogs.dialoginterface.DialogButtonListener;
import org.bavariancoders.raspberryled.adapter.ButtonClickListener;
import org.bavariancoders.raspberryled.adapter.ScriptRecyclerViewAdapter;
import org.bavariancoders.raspberryled.dialogs.ColorPickerDialog;
import org.bavariancoders.raspberryled.dialogs.InterpolateDialogFragment;
import org.bavariancoders.raspberryled.dialogs.SaveScriptDialog;
import org.bavariancoders.raspberryled.dialogs.dialoginterface.InterpolateButtonClickListener;
import org.bavariancoders.raspberryled.dialogs.dialoginterface.SeekBarListener;
import org.bavariancoders.raspberryled.dialogs.TimeChooserDialogFragment;
import org.bavariancoders.raspberryled.model.Container;
import org.bavariancoders.raspberryled.model.Device;
import org.bavariancoders.raspberryled.model.DeviceContainer;
import org.bavariancoders.raspberryled.model.InterpolateElem;
import org.bavariancoders.raspberryled.model.LinkUnlinkException;
import org.bavariancoders.raspberryled.model.ScriptElem;
import org.bavariancoders.raspberryled.model.ScriptListContainer;
import org.bavariancoders.raspberryled.model.SetElem;
import org.bavariancoders.raspberryled.model.ScriptContainer;
import org.bavariancoders.raspberryled.model.WaitElem;
import org.bavariancoders.raspberryled.net.LEDCommandSender;
import org.bavariancoders.raspberryled.storage.DataStorage;
import org.bavariancoders.raspberryled.storage.DataStorageException;
import org.bavariancoders.raspberryled.storage.ScriptDbStorage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by BavarianCoders on 04.03.17
 */

public class ScriptMain extends AppCompatActivity implements View.OnClickListener{

    private int currentTime = 0;
    private int interpolateTime = 0;
    private int interpolateSteps = 0;
    private int interpolateColorFrom = 0;
    private int interpolateColorTo = 0;
    private int moveLast = 0;
    private boolean moveSelected = false;
    private int choosenScriptIntentMessage = 0;
    private List<Integer> choosenDevicesIdList = new ArrayList<Integer>();
    private Device[] choosenDevicesArray;

    public static String currentName = "New Script";
    public static int currentId;
    public static int color = 0;
    private RecyclerView mRecyclerView;
    private TimeChooserDialogFragment mTimeChooserDialog;
    private InterpolateDialogFragment mInterpolateDialog;
    private ScriptRecyclerViewAdapter adapter;
    private Container<ScriptContainer> scriptListcontainer = ScriptListContainer.instance();
    private ScriptContainer container = null;
    private Container<Device> deviceContainer = DeviceContainer.instance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.skript_main);
        Button b = (Button) findViewById(R.id.skript_set);
        b.setOnClickListener(this);
        b = (Button) findViewById(R.id.skript_wait);
        b.setOnClickListener(this);
        b = (Button) findViewById(R.id.skript_interpolate);
        b.setOnClickListener(this);
        b = (Button) findViewById(R.id.skript_run);
        b.setOnClickListener(this);

        Intent intent = this.getIntent();
        choosenScriptIntentMessage = intent.getIntExtra(ConstantsFunctions.CHOOSEN_SCRIPT_ID, 0);
        choosenDevicesIdList = intent.getIntegerArrayListExtra(ConstantsFunctions.CHOOSEN_DEVICE_IDS);
        container = scriptListcontainer.getLinkElement(choosenScriptIntentMessage);
        choosenDevicesArray = new Device[choosenDevicesIdList.size()];
        for (int i : choosenDevicesIdList)
            choosenDevicesArray[choosenDevicesIdList.indexOf(i)] = deviceContainer.getLinkElement(i);

        Toolbar topToolBar = (Toolbar)findViewById(R.id.script_toolbar);
        setSupportActionBar(topToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.skript_text);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new ScriptRecyclerViewAdapter(container);

        adapter.setOnDeleteButtonListener(new ButtonClickListener() {
            @Override
            public void onButtonClick(int position, View v) {
                try {
                    container.unlinkElement(container.getLinkElement(position));
                    adapter.notifyItemRemoved(position);
                    container.restoreswap();
                } catch (LinkUnlinkException e) {
                    e.printStackTrace();
                }

                moveSelected = false;
            }
        });

        adapter.setOnMoveButtonListener(new ButtonClickListener() {
            @Override
            public void onButtonClick(int position, View v) {
                if (!moveSelected) {
                    moveSelected = true;
                    moveLast = position;
                    container.getLinkElement(position).setSwapSet(true);
                    adapter.notifyDataSetChanged();
                }else{
                    moveSelected = false;
                    container.restoreswap();
                    Collections.swap(container.getScriptList(), moveLast , position);
                    adapter.notifyItemMoved(moveLast, position);
                }
            }
        });

        adapter.setOnClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(final int position, View v) {
                final ScriptElem elem = container.getLinkElement(position);
                if (elem instanceof SetElem) {
                    new ColorPickerDialog(ScriptMain.this, ((SetElem) elem).get_color(), new ColorPickerDialog.OnColorSelectedListener() {
                        @Override
                        public void onColorSelected(int color) {
                            try {
                                container.replaceElement(position, new SetElem(color));
                                adapter.notifyItemChanged(position);
                            } catch (LinkUnlinkException e) {
                                e.printStackTrace();
                            }
                        }
                    }).show();

                } else if (elem instanceof WaitElem) {
                    currentTime = ((WaitElem) elem).get_Time();
                    mTimeChooserDialog = TimeChooserDialogFragment.newInstance(((WaitElem) elem).get_Time());
                    mTimeChooserDialog.setOnSeekBarListener(new SeekBarListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                            currentTime = ConstantsFunctions.timeInt(progress);
                            mTimeChooserDialog.setProgressMessage("Selected Time: "+ConstantsFunctions.timeToString(currentTime)+"sec");
                        }
                    });

                    mTimeChooserDialog.setOnTimeDialogClickListener(new DialogButtonListener() {
                        @Override
                        public void onDialogButtonPositiveClick(DialogFragment dialog) {
                            try {
                                container.replaceElement(position, new WaitElem(currentTime));
                                adapter.notifyItemChanged(position);
                                dialog.dismiss();
                            } catch (LinkUnlinkException e) {
                                e.printStackTrace();
                                dialog.dismiss();
                            }
                        }

                        @Override
                        public void onDialogButtonNegativeClick(DialogFragment dialog) {
                            dialog.dismiss();
                        }
                    });

                    mTimeChooserDialog.show(getSupportFragmentManager(), "Choose Time");
                } else if (elem instanceof InterpolateElem) {
                    InterpolateElem interElem = (InterpolateElem) elem;
                    interpolateColorFrom = interElem.get_start_color();
                    interpolateColorTo = interElem.get_end_color();
                    interpolateTime = interElem.get_time();
                    interpolateSteps = interElem.get_steps();
                    mInterpolateDialog =
                            InterpolateDialogFragment.newInstance(new InterpolateHandler('m', position),
                                    interElem.get_time(), interElem.get_steps(),
                                    interElem.get_start_color(), interElem.get_end_color());

                    mInterpolateDialog.show(getSupportFragmentManager(), "Choose gradient");
                }
            }

            @Override
            public void onItemLongClick(int position, View v) {

            }
        });
        mRecyclerView.setAdapter(adapter);

        ItemTouchHelper.Callback callback = new ItemTouchHelperCallback(adapter);
        final ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(mRecyclerView);

        adapter.setOnStartDragListener(new OnStartDragListener() {
            @Override
            public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
                touchHelper.startDrag(viewHolder);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.toolbar_script, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_script:
                final SaveScriptDialog save = SaveScriptDialog.newInstance(container.getName());
                save.setOnSaveClickListener(new DialogButtonListener() {
                    @Override
                    public void onDialogButtonPositiveClick(DialogFragment dialog) {
                        DataStorage<ScriptContainer> scriptStorage = ScriptDbStorage.getInstance(ScriptMain.this);
                        container.setName(save.getName());
                        try {
                            scriptStorage.save(container);
                        } catch (DataStorageException e) {
                            e.printStackTrace();
                        }

                        dialog.dismiss();
                    }

                    @Override
                    public void onDialogButtonNegativeClick(DialogFragment dialog) {
                        dialog.dismiss();
                    }
                });

                save.show(getSupportFragmentManager(), "Save Script");

        }

        return super.onOptionsItemSelected(item);
    }

    public void saveScript(View view){
        final SaveScriptDialog save = new SaveScriptDialog();

        save.setOnSaveClickListener(new DialogButtonListener() {
            @Override
            public void onDialogButtonPositiveClick(DialogFragment dialog) {
                Intent i = new Intent(ScriptMain.this, ColorChooserActivity.class);

                // todo

                startActivity(i);
                dialog.dismiss();
            }

            @Override
            public void onDialogButtonNegativeClick(DialogFragment dialog) {
                dialog.dismiss();
            }
        });

        save.show(getSupportFragmentManager(), "SaveDialog");



    }
    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.skript_set:
                get_Color();
                break;
            case R.id.skript_wait:
                mTimeChooserDialog = new TimeChooserDialogFragment();

                mTimeChooserDialog.setOnSeekBarListener(new SeekBarListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        currentTime = ConstantsFunctions.timeInt(progress);
                        mTimeChooserDialog.setProgressMessage("Selected Time: "+ConstantsFunctions.timeToString(currentTime)+"sec");
                    }
                });

                mTimeChooserDialog.setOnTimeDialogClickListener(new DialogButtonListener() {
                    @Override
                    public void onDialogButtonPositiveClick(DialogFragment dialog) {
                        try {
                            container.linkElement(new WaitElem(currentTime));
                            adapter.notifyItemInserted(adapter.getItemCount() - 1);
                            dialog.dismiss();
                        } catch (LinkUnlinkException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onDialogButtonNegativeClick(DialogFragment dialog) {
                        dialog.dismiss();
                    }
                });

                mTimeChooserDialog.show(getSupportFragmentManager(), "timeDialog");

                break;
            case R.id.skript_interpolate:
                mInterpolateDialog = InterpolateDialogFragment.newInstance(new InterpolateHandler());

                mInterpolateDialog.show(getSupportFragmentManager(), "InterpolateDialog");

                break;
            case R.id.skript_run:
                String message = container.getSignal();
                String jsonMessage = container.getJSONSignal();
                new LEDCommandSender(jsonMessage, choosenDevicesArray).start(choosenDevicesArray.length);
                Toast.makeText(getApplicationContext(),"Send:"+ container.getSignal()+"",Toast.LENGTH_LONG).show();
                break;

        }
    }

    private void get_Color(){
        ColorPickerDialog colorPickerDialog = new ColorPickerDialog(this, color, new ColorPickerDialog.OnColorSelectedListener() {

            @Override
            public void onColorSelected(int color) {
                SetElem neu = new SetElem(color);
                try {
                    container.linkElement(neu);
                    adapter.notifyItemInserted(adapter.getItemCount() - 1);
                    Toast.makeText(getApplicationContext(),color+neu.kindtoText()+neu.paramtoText(),Toast.LENGTH_LONG).show();
                } catch (LinkUnlinkException e) {
                    Toast.makeText(getApplicationContext(),e.getLocalizedMessage(),Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }

        });
        colorPickerDialog.show();
    }

    private class InterpolateHandler implements DialogButtonListener, InterpolateButtonClickListener,
            SeekBarListener.StepSeekBarListener, SeekBarListener.TimeSeekBarListener{
        // modes: 'a' is add element, 'm' is modify element
        private char mode = 'a';
        private int position = -1;

        InterpolateHandler () {}

        InterpolateHandler (char mode, int position) {
            this.mode = mode;
            this.position = position;
        }

        InterpolateHandler (int position) {
            this.position = position;
        }


        @Override
        public void onDialogButtonPositiveClick(DialogFragment dialog) {
            try {
                if (mode == 'm') {
                    container.replaceElement(position, new InterpolateElem(interpolateColorFrom, interpolateColorTo, interpolateSteps, interpolateTime));
                    adapter.notifyItemChanged(position);
                    dialog.dismiss();
                    return;
                } else {
                    container.linkElement(new InterpolateElem(interpolateColorFrom, interpolateColorTo, interpolateSteps, interpolateTime));
                    adapter.notifyItemInserted(adapter.getItemCount() - 1);
                }
                dialog.dismiss();
            } catch (LinkUnlinkException e) {
                e.printStackTrace();
                dialog.dismiss();
            }

        }

        @Override
        public void onDialogButtonNegativeClick(DialogFragment dialog) {
            dialog.dismiss();
        }

        @Override
        public void onClickColorFromButton(View v) {
            int color = Color.rgb(0, 0, 0);
            if (position != -1)
                color = ((InterpolateElem) container.getLinkElement(position)).get_start_color();
            ColorPickerDialog picker = new ColorPickerDialog(mInterpolateDialog.getActivity(), color, new ColorPickerDialog.OnColorSelectedListener() {
                @Override
                public void onColorSelected(int color) {
                    interpolateColorFrom = color;
                    mInterpolateDialog.setColorFromBackground(color);
                }
            });
            picker.show();
        }

        @Override
        public void onClickColorToButton(View v) {
            int color = Color.rgb(0, 0, 0);
            if (position != -1)
                color = ((InterpolateElem) container.getLinkElement(position)).get_end_color();
            ColorPickerDialog picker = new ColorPickerDialog(mInterpolateDialog.getActivity(), color, new ColorPickerDialog.OnColorSelectedListener() {
                @Override
                public void onColorSelected(int color) {
                    interpolateColorTo = color;
                    mInterpolateDialog.setColorToBackground(color);
                }
            });
            picker.show();
        }

        @Override
        public void onTimeProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            interpolateTime = progress;
            mInterpolateDialog.setTimeSeekProgress("Time: "+ progress);
        }

        @Override
        public void onStepProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            interpolateSteps = progress;
            mInterpolateDialog.setStepsSeekProgress("Steps: "+ progress);
        }
    }

}
