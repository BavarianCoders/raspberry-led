/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled;


import android.graphics.Color;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by BavarianCoders on 04.03.17
 */

public class ConstantsFunctions {
    private ConstantsFunctions() {}

    // Intents
    public static final String EXTRA_MESSAGE = "MainActivity.IPPORT";
    public static final String SCRIPT_COMMAND = "MainActivity.SCRIPT_COMMAND";
    public static final String DEVICE_LIST = "MainActivity.DEVICE_LIST";
    public static final String CHOOSEN_SCRIPT_ID = "ColorChooserActivity.CHOOSEN_SCRIPT_ID";
    public static final String CHOOSEN_DEVICE_IDS = "ColorChooserActivity.CHOSEN_DEVICE_IDS";

    // Xml Stuff
    public static final String DEVICE_XML_PATH = "devicelist.xml";
    public static final String DEVICE_TAG = "device";
    public static final String IP_TAG = "ip";
    public static final String PORT_TAG = "port";
    public static final String NAME_TAG = "name";
    public static final String LASTCOLOR_TAG = "lastcolor";

    // Socket constants
    public static final int TIMEOUT = 2000;
    public static final int TIMEOUT_SHORT = 500;

    // JSON
    public static final String LIGHTEFFECT_ARRAY = "light_effect";
    public static final String SETELEM_OBJECT = "{setcolor:{}}";
    public static final String SET_VALUE = "setcolor";
    public static final String WAITELEM_OBJECT = "{wait:{}}";
    public static final String WAIT_VALUE = "wait";
    public static final String INTERPOLATEELEM_OBJECT = "{transition:{startcolor:{},endcolor:{}}}";
    public static final String INTERPOLATE_VALUE = "transition";
    public static final String STARTCOLOR_OBJECT = "startcolor";
    public static final String ENDCOLOR_OBJECT = "endcolor";
    public static final String VALUE_RED = "r";
    public static final String VALUE_GREEN = "g";
    public static final String VALUE_BLUE = "b";
    public static final String VALUE_TIME = "time";
    public static final String VALUE_STEPS = "steps";

    public static String getFormatedRGB (int color) {
        return String.format("%3d", Color.red(color)) + String.format("%3d", Color.green(color))
                + String.format("%3d", Color.blue(color));
    }

    public static String getFormatedRGB (int r, int g, int b) {
        return String.format("%3d", r) + String.format("%3d", g) + String.format("%3d", b);
    }

    public static String getJsonRgb (int r, int g, int b) {
        try {
            JSONObject jsonObject = new JSONObject("{setcolor:{}}");
            jsonObject.getJSONObject("setcolor").put("r", r);
            jsonObject.getJSONObject("setcolor").put("g", g);
            jsonObject.getJSONObject("setcolor").put("b", b);
            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int timeInt(int progress){
        int sens = 7;
        double zw = (double)(progress)/100;
        for (int i = 0; i < sens; i++)
            zw *= (double)(progress)/100;
        return (int)(1+99999998*zw);
    }

    public static int timeToProgress (int time) {
        int sens = 7;
        double zw = (double) time / 99999998d;
        zw = Math.pow(zw, 1d/7d);
        return (int)(zw * 100);
    }

    public static String timeToString(int time){
        return ""+ (time-time%10000000)/10000000+ (time%10000000-time%1000000)/1000000+ (time%1000000-time%100000)/100000+ (time%100000-time%10000)/10000+ "."+(time%10000-time%1000)/1000+ (time%1000-time%100)/100+ (time%100-time%10)/10+ (time%10);
    }
}
