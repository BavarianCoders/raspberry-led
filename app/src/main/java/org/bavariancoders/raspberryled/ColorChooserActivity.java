/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import org.bavariancoders.raspberryled.adapter.ButtonClickListener;
import org.bavariancoders.raspberryled.adapter.ItemClickListener;
import org.bavariancoders.raspberryled.adapter.ScriptListRecyclerViewAdapter;
import org.bavariancoders.raspberryled.model.Container;
import org.bavariancoders.raspberryled.model.DeviceContainer;
import org.bavariancoders.raspberryled.model.Device;
import org.bavariancoders.raspberryled.model.LinkUnlinkException;
import org.bavariancoders.raspberryled.model.ScriptContainer;
import org.bavariancoders.raspberryled.model.ScriptListContainer;
import org.bavariancoders.raspberryled.net.LEDCommandSender;
import org.bavariancoders.raspberryled.net.ServerOnlineAsyncTask;
import org.bavariancoders.raspberryled.storage.DataStorage;
import org.bavariancoders.raspberryled.storage.DataStorageException;
import org.bavariancoders.raspberryled.storage.ScriptDbStorage;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by BavarianCoders on 04.03.17
 */

public class ColorChooserActivity extends AppCompatActivity {
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private RecyclerView recyclerView;
    private ColorPicker picker;
    private SwipeRefreshLayout swiperefresh;
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private TextInputLayout redTextLayout, blueTextLayout, greenTextLayout;
    private int color;
    private int red = 0, green = 0, blue = 0;
    private static int message;
    private String scriptCommand;
    private List<Integer> deviceIDList = new ArrayList<Integer>();
    private List<Device> devicesList = new ArrayList<Device>();
    private Device[] deviceArray;

    private static List<Integer> deviceId = new ArrayList<Integer>();
    private Container<Device> deviceCon = DeviceContainer.instance();
    private Container<ScriptContainer> scriptListContainer = ScriptListContainer.instance();

    public void startSkriptActivity(View view) {
        Intent intent = new Intent(ColorChooserActivity.this, ScriptMain.class);
        startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.colorchooser);

        Intent intent = this.getIntent();
        deviceIDList.clear();
        devicesList.clear();

        loadScriptsFromDB();

        initRgbTextLayouts();

        redTextLayout.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                                   @Override
                                                   public void onFocusChange(View view, boolean b) {
                                                       checkRGB();
                                                   }
        });

        greenTextLayout.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange (View view, boolean b) {
                checkRGB();
            }
        });

                recyclerView = (RecyclerView) findViewById(R.id.skript_text);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        final ScriptListRecyclerViewAdapter adapter = new ScriptListRecyclerViewAdapter();
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        adapter.setOnItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                if (position == 0) {
                    Intent intent = new Intent(ColorChooserActivity.this, ScriptMain.class);
                    intent.putIntegerArrayListExtra(ConstantsFunctions.CHOOSEN_DEVICE_IDS, (ArrayList<Integer>) deviceIDList);
                    startActivity(intent);
                }
                String signal = scriptListContainer.getLinkElement(position).getJSONSignal();
                new LEDCommandSender(signal, deviceArray).start(deviceArray.length);
            }

            @Override
            public void onItemLongClick(int position, View v) {
                // todo
            }
        });

        adapter.setOnDeleteButtonClickListener(new ButtonClickListener() {
            @Override
            public void onButtonClick(int position, View v) {
                try {
                    scriptListContainer.unlinkElement(scriptListContainer.getLinkElement(position));
                    DataStorage<ScriptContainer> dataStorage = ScriptDbStorage.getInstance(ColorChooserActivity.this);
                    dataStorage.delete(scriptListContainer.getLinkElement(position));
                    adapter.notifyItemRemoved(position);
                } catch (LinkUnlinkException | DataStorageException e) {
                    e.printStackTrace();
                }
            }
        });

        adapter.setOnLoadButtonClickListener(new ButtonClickListener() {
            @Override
            public void onButtonClick(int position, View v) {
                Intent intent = new Intent(ColorChooserActivity.this, ScriptMain.class);
                intent.putExtra(ConstantsFunctions.CHOOSEN_SCRIPT_ID, position);
                intent.putIntegerArrayListExtra(ConstantsFunctions.CHOOSEN_DEVICE_IDS, (ArrayList<Integer>) deviceIDList);
                startActivity(intent);
            }
        });

        picker = (ColorPicker) findViewById(R.id.colorPicker);
        swiperefresh = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        toolbar = (Toolbar) findViewById(R.id.color_toolbar);
        setSupportActionBar(toolbar);

        message = intent.getIntExtra(ConstantsFunctions.EXTRA_MESSAGE, -1);
        scriptCommand = intent.getStringExtra(ConstantsFunctions.SCRIPT_COMMAND);
        deviceIDList = intent.getIntegerArrayListExtra(ConstantsFunctions.DEVICE_LIST);
        if (deviceIDList != null) {
            for (Integer i : deviceIDList)
                devicesList.add((Device) deviceCon.getLinkElement(i));

            deviceArray =  new Device[deviceIDList.size()];
            deviceArray = devicesList.toArray(deviceArray);
            deviceId = deviceIDList;
            new ServerOnlineAsyncTask(this, swiperefresh).execute(deviceArray);
        }
        else if(message == -1) {
            deviceIDList = deviceId;
            for (Integer i : deviceIDList)
                devicesList.add((Device) deviceCon.getLinkElement(i));
            deviceArray =  new Device[deviceIDList.size()];
            deviceArray = devicesList.toArray(deviceArray);
            new LEDCommandSender(scriptCommand, deviceArray).start(deviceArray.length);

        }
        else {
            deviceIDList = new ArrayList<Integer>();
            deviceIDList.add(message);
            for (Integer i : deviceIDList)
                devicesList.add((Device) deviceCon.getLinkElement(i));
            deviceArray =  new Device[deviceIDList.size()];
            deviceArray = devicesList.toArray(deviceArray);
            new ServerOnlineAsyncTask(this, swiperefresh).execute(deviceCon.getLinkElement(message));
            deviceId = deviceIDList;
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new ServerOnlineAsyncTask(ColorChooserActivity.this, swiperefresh).executeOnExecutor(ServerOnlineAsyncTask.THREAD_POOL_EXECUTOR, deviceArray);
            }
        });

        picker.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                color = picker.getColor();
                String rgbJson = ConstantsFunctions.getJsonRgb(Color.red(color), Color.green(color), Color.blue(color));

                redTextLayout.getEditText().setText(Integer.toString(Color.red(color)));
                blueTextLayout.getEditText().setText(Integer.toString(Color.blue(color)));
                greenTextLayout.getEditText().setText(Integer.toString(Color.green(color)));

                LEDCommandSender sender = new LEDCommandSender(rgbJson, deviceArray);
                ArrayList<Future<Object>> list = (ArrayList<Future<Object>>) sender.start(deviceArray.length);
                for (Future<Object> f : list)
                    try {
                        f.get();
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                    }
                return false;
            }
        });
    }

    private void loadScriptsFromDB() {
        scriptListContainer.clearContainer();
        DataStorage<ScriptContainer> scriptStorage = ScriptDbStorage.getInstance(this);
        try {
            scriptStorage.loadAll(scriptListContainer);
        } catch (DataStorageException e) {
            e.printStackTrace();
        }
        scriptStorage.close();
    }

    private void initRgbTextLayouts (){
        redTextLayout = (TextInputLayout) findViewById(R.id.input_red);
        blueTextLayout = (TextInputLayout) findViewById(R.id.input_blue);
        greenTextLayout = (TextInputLayout) findViewById(R.id.input_green);

        redTextLayout.setErrorEnabled(false);
        greenTextLayout.setErrorEnabled(false);
        blueTextLayout.setErrorEnabled(false);
    }

    private void checkRGB () {
        try {
            red = Integer.parseInt(redTextLayout.getEditText().getText().toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            red = 0;
        }
        try {
            green = Integer.parseInt(greenTextLayout.getEditText().getText().toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            green = 0;
        }
        try {
            blue = Integer.parseInt(blueTextLayout.getEditText().getText().toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            blue = 0;
        }
        if (red > 255) {
            redTextLayout.setError(getString(R.string.rgb_out_of_bounds));
            redTextLayout.setErrorEnabled(true);
        }
        else
            redTextLayout.setErrorEnabled(false);
        if (green > 255) {
            greenTextLayout.setError(getString(R.string.rgb_out_of_bounds));
            greenTextLayout.setErrorEnabled(true);
        }
        else
            greenTextLayout.setErrorEnabled(false);
        if (blue > 255) {
            blueTextLayout.setError(getString(R.string.rgb_out_of_bounds));
            blueTextLayout.setErrorEnabled(true);
        }
        else
            blueTextLayout.setErrorEnabled(false);
    }

    public void sendRGB (View view) {
        checkRGB();
        String rgbString = ConstantsFunctions.getJsonRgb(red, green, blue);
        new LEDCommandSender(rgbString, deviceArray).start(deviceArray.length);
        picker.setColor(Color.rgb(red, green, blue));
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        getMenuInflater().inflate(R.menu.colorchooser_toolbar, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        switch (item.getItemId()) {
            case R.id.toggle_drawer:
                if (drawerLayout.isDrawerOpen(findViewById(R.id.nav_view)))
                    drawerLayout.closeDrawer(findViewById(R.id.nav_view));
                drawerLayout.openDrawer(findViewById(R.id.nav_view));
                break;
            case R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
