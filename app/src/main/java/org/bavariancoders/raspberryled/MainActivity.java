/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import org.bavariancoders.raspberryled.fragments.AboutFragment;
import org.bavariancoders.raspberryled.fragments.BuildBreadbordFragment;
import org.bavariancoders.raspberryled.fragments.MainFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BavarianCoders on 07.03.17
 */

public class MainActivity extends AppCompatActivity implements MainFragment.OnDeviceListListener, FragmentManager.OnBackStackChangedListener {
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     *
     */
    private Toolbar toolbar;
    private Drawer mDrawer;

    private static final int ID_HOME = 1;
    private static final int ID_BUILDBREADBORD = 2;
    private static final int ID_ABOUT = 3;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);

        mDrawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.home).withIcon(GoogleMaterial.Icon.gmd_home)
                                .withIdentifier(ID_HOME).withSelectable(false),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName(R.string.buildbreadbord).withIcon(GoogleMaterial.Icon.gmd_assignment)
                                .withIdentifier(ID_BUILDBREADBORD).withSelectable(false),
                        new PrimaryDrawerItem().withName(R.string.about).withIcon(GoogleMaterial.Icon.gmd_group)
                                .withIdentifier(ID_ABOUT).withSelectable(false)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int i, IDrawerItem drawerItem) {
                        if (drawerItem != null) {
                            PrimaryDrawerItem item = (PrimaryDrawerItem) drawerItem;
                            Intent intent = null;
                            switch ((int) item.getIdentifier()) {
                                case ID_BUILDBREADBORD:
                                    onHelpSelected();
                                    break;
                                case ID_HOME:
                                    onHomeSelected();
                                    break;
                                case ID_ABOUT:
                                    onAboutSelected();
                            }
                        }
                        return false;
                    }
                })
        .build();

        getSupportFragmentManager().addOnBackStackChangedListener(this);

        if (savedInstanceState != null) return;

        onHomeSelected();
    }

    @Override
    public void onDeviceItemSelected(List<Integer> deviceIds) {
        Intent intent = new Intent(this, ColorChooserActivity.class);
        intent.putIntegerArrayListExtra(ConstantsFunctions.DEVICE_LIST, (ArrayList<Integer>) deviceIds);
        startActivity(intent);
    }

    private void setFragment(Fragment fragment, boolean addToBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.fragment_container, fragment);
        if (addToBackStack) {
            ft.addToBackStack(null);
        }
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();

    }

    private void onHomeSelected () {
        toolbar.setTitle(R.string.app_name);
        mDrawer.setSelection(ID_HOME, false);
        Fragment fragment = new MainFragment();
        setFragment(fragment, false);
    }

    private void onHelpSelected() {
        toolbar.setTitle(R.string.buildbreadbord);
        mDrawer.setSelection(ID_BUILDBREADBORD, false);
        Fragment fragment = new BuildBreadbordFragment();
        setFragment(fragment, true);
    }

    private void onAboutSelected() {
        toolbar.setTitle(R.string.about);
        mDrawer.setSelection(ID_ABOUT, false);
        Fragment frag = new AboutFragment();
        setFragment(frag, true);
    }

    @Override
    public void onBackPressed() {
        if (mDrawer != null && mDrawer.isDrawerOpen()) {
            mDrawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onBackStackChanged() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager == null) return;

        Fragment fragment = manager.findFragmentById(R.id.fragment_container);

        if (fragment == null) return;

        if (fragment instanceof MainFragment) {
            toolbar.setTitle(R.string.app_name);
            mDrawer.setSelection(ID_HOME, false);
        }
        else if (fragment instanceof BuildBreadbordFragment) {
            toolbar.setTitle(R.string.buildbreadbord);
            mDrawer.setSelection(ID_BUILDBREADBORD, false);
        }
        else if (fragment instanceof AboutFragment) {
            toolbar.setTitle(R.string.about);
            mDrawer.setSelection(ID_ABOUT, false);
        }
    }
}