/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import org.bavariancoders.raspberryled.ConstantsFunctions;
import org.bavariancoders.raspberryled.dialogs.dialoginterface.DialogButtonListener;
import org.bavariancoders.raspberryled.dialogs.dialoginterface.SeekBarListener;

/**
 * Created by BavarianCoders on 06.03.17
 */

public class TimeChooserDialogFragment extends DialogFragment {
    private DialogButtonListener timeChooserListener;
    private SeekBarListener seekBarListener;
    private TextView seekBarProgress;
    private int time = 0;

    public static TimeChooserDialogFragment newInstance (int time) {
        TimeChooserDialogFragment fragment = new TimeChooserDialogFragment();
        fragment.time = time;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstances) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        final SeekBar seek = new SeekBar(getActivity());
        seekBarProgress = new TextView(getActivity());
        seekBarProgress.setText("Time: " + ConstantsFunctions.timeToString(time) + "sec");
        layout.addView(seekBarProgress);
        layout.addView(seek);

        seek.setMax(100);
        seek.setProgress(ConstantsFunctions.timeToProgress(time));
        builder.setView(layout);

        builder.setTitle("Select Time")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        timeChooserListener.onDialogButtonPositiveClick(TimeChooserDialogFragment.this);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        timeChooserListener.onDialogButtonNegativeClick(TimeChooserDialogFragment.this);
                    }
                });

        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekBarListener.onProgressChanged(seekBar, progress, fromUser);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        return builder.create();
    }

    public void setOnSeekBarListener (SeekBarListener seekBarListener) {
        this.seekBarListener = seekBarListener;
    }

    public void setOnTimeDialogClickListener (DialogButtonListener listener) {
        this.timeChooserListener = listener;
    }

    public void setProgressMessage (String message) {
        seekBarProgress.setText(message);
    }
}
