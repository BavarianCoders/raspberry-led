/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.widget.EditText;
import android.widget.LinearLayout;

import org.bavariancoders.raspberryled.dialogs.dialoginterface.DialogButtonListener;

/**
 * Created by BavarianCoders on 22.03.17
 */

public class SaveScriptDialog extends DialogFragment {
    private DialogButtonListener saveListener;
    private EditText name;
    private String scriptName = "New Script";

    public SaveScriptDialog () {}

    public static SaveScriptDialog newInstance(String name) {
        SaveScriptDialog saveScriptDialog = new SaveScriptDialog();
        if (name != null)
            saveScriptDialog.scriptName = name;
        return saveScriptDialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstances) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        name = new EditText(getActivity());
        name.setText(scriptName);
        layout.addView(name);


        builder.setView(layout);
        builder.setTitle("Save");
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                saveListener.onDialogButtonPositiveClick(SaveScriptDialog.this);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                saveListener.onDialogButtonNegativeClick(SaveScriptDialog.this);
            }
        });

        return builder.create();
    }

    public void setOnSaveClickListener (DialogButtonListener listener) {
        this.saveListener = listener;
    }

    public String getName(){
        return name.getText().toString();
    }

    public void setName(String name) {this.name.setText(name);}

}
