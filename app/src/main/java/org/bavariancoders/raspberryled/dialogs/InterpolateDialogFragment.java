/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import org.bavariancoders.raspberryled.dialogs.dialoginterface.DialogButtonListener;
import org.bavariancoders.raspberryled.dialogs.dialoginterface.InterpolateButtonClickListener;
import org.bavariancoders.raspberryled.dialogs.dialoginterface.SeekBarListener;


/**
 * Created by BavarianCoders on 10.03.17
 */

public class InterpolateDialogFragment extends DialogFragment {
    private DialogButtonListener buttonListener;
    private InterpolateButtonClickListener interpolateClickListener;
    private SeekBarListener.TimeSeekBarListener seekTimeListener;
    private SeekBarListener.StepSeekBarListener seekStepsListener;

    private TextView time , steps, colorFrom, colorTo;
    private Button colorFromButton, colorToButton;
    private SeekBar seekTime, seekSteps;

    int timeProgress = 0, stepProgress = 0, colorValueFrom = 0, colorValueto = 0;

    public static InterpolateDialogFragment newInstance (DialogButtonListener dialogButtonListener,
                                                         InterpolateButtonClickListener interpolateButtonClickListener,
                                                         SeekBarListener.TimeSeekBarListener seekTimeListener,
                                                         SeekBarListener.StepSeekBarListener seekStepsListener) {
        InterpolateDialogFragment fragment = new InterpolateDialogFragment();
        fragment.buttonListener = dialogButtonListener;
        fragment.interpolateClickListener = interpolateButtonClickListener;
        fragment.seekTimeListener = seekTimeListener;
        fragment.seekStepsListener = seekStepsListener;

        return fragment;
    }

    public static <T extends DialogButtonListener & InterpolateButtonClickListener &
            SeekBarListener.TimeSeekBarListener & SeekBarListener.StepSeekBarListener>
    InterpolateDialogFragment newInstance (T listener, int timeProgress, int stepProgress, int colorFrom, int colorTo) {
        InterpolateDialogFragment fragment = new InterpolateDialogFragment();
        fragment.buttonListener = listener;
        fragment.seekTimeListener = listener;
        fragment.seekStepsListener = listener;
        fragment.interpolateClickListener = listener;
        fragment.timeProgress = timeProgress;
        fragment.stepProgress = stepProgress;
        fragment.colorValueFrom = colorFrom;
        fragment.colorValueto = colorTo;
        return fragment;
    }

    public static <T extends DialogButtonListener & InterpolateButtonClickListener &
            SeekBarListener.TimeSeekBarListener & SeekBarListener.StepSeekBarListener>
    InterpolateDialogFragment newInstance (T listener) {
        InterpolateDialogFragment fragment = new InterpolateDialogFragment();
        fragment.buttonListener = listener;
        fragment.seekTimeListener = listener;
        fragment.seekStepsListener = listener;
        fragment.interpolateClickListener = listener;
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog (@NonNull Bundle savedInstances) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        time = new TextView(getActivity());
        steps = new TextView(getActivity());
        colorFrom = new TextView(getActivity());
        colorTo = new TextView(getActivity());

        colorFromButton = new Button(getActivity());
        colorToButton = new Button(getActivity());

        seekTime = new SeekBar(getActivity());
        seekSteps = new SeekBar(getActivity());

        LinearLayout layout = new LinearLayout(getActivity());
        initLayout(layout);

        builder.setView(layout);

        builder.setTitle("Choose values for RGB transitions")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        buttonListener.onDialogButtonPositiveClick(InterpolateDialogFragment.this);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        buttonListener.onDialogButtonNegativeClick(InterpolateDialogFragment.this);
                    }
                });

        colorFromButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interpolateClickListener.onClickColorFromButton(v);
            }
        });

        colorToButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interpolateClickListener.onClickColorToButton(v);
            }
        });

        seekTime.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekTimeListener.onTimeProgressChanged(seekBar, progress, fromUser);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        seekSteps.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekStepsListener.onStepProgressChanged(seekBar, progress, fromUser);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        return builder.create();
    }

    public void setOnInterpolateButtonClickListener (InterpolateButtonClickListener listener) {
        this.interpolateClickListener = listener;
    }

    public void setOnDialogClickListener(DialogButtonListener listener) {
        this.buttonListener = listener;
    }

    public void setOnTimeSeekChanged (SeekBarListener.TimeSeekBarListener listener) {
        this.seekTimeListener = listener;
    }

    public void setOnStepsSeekChanged (SeekBarListener.StepSeekBarListener listener) {
        this.seekStepsListener = listener;
    }

    public void setTimeSeekProgress (String progress) {
        this.time.setText(progress);
    }

    public void setStepsSeekProgress (String progress) {
        this.steps.setText(progress);
    }

    public void setColorFromBackground (int color) {
        colorFromButton.setBackgroundColor(color);
    }

    public void setColorToBackground (int color) {
        colorToButton.setBackgroundColor(color);
    }

    private void initLayout(LinearLayout layout) {
        layout.setOrientation(LinearLayout.VERTICAL);

        colorFrom.setText("Start color:");
        colorTo.setText("Finish color:");
        time.setText("Time: " + timeProgress);
        steps.setText("Steps: " + stepProgress);

        colorFromButton.setText("Change");
        colorToButton.setText("Change");
        if (colorValueFrom != 0)
            colorFromButton.setBackgroundColor(colorValueFrom);
        if (colorValueto != 0)
            colorToButton.setBackgroundColor(colorValueto);

        layout.addView(colorFrom);
        layout.addView(colorFromButton);
        layout.addView(colorTo);
        layout.addView(colorToButton);
        layout.addView(time);
        layout.addView(seekTime);
        layout.addView(steps);
        layout.addView(seekSteps);
        seekTime.setMax(100);
        seekSteps.setMax(255);
        seekTime.setProgress(timeProgress);
        seekSteps.setProgress(stepProgress);
    }
}
