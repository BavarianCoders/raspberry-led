/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import org.bavariancoders.raspberryled.BuildConfig;
import org.bavariancoders.raspberryled.R;

/**
 * Created by BavarianCoders on 28.09.17
 */

public class AboutFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        String aboutHtml = "<html>" +
                "<head></head>" +
                    "<body>" +
                        getString(R.string.code_html) + ": <a href=\"https://bitbucket.org/BavarianCoders/raspberryled/src\">Bitbucket</a><br>" +
                        getString(R.string.license_html) + ": GNU Public License v3" +
                        "<h3>" + getString(R.string.devs_html) + "</h3>" +
                            "<ul>" +
                                "<li>Max</li>" +
                                "<li>Thomas</li>" +
                            "</ul>" +
                        "<h3>" + getString(R.string.libs_html) + "</h3>" +
                            "<ul>" +
                                "<li><a href=\"https://developer.android.com/topic/libraries/support-library/index.html\">Android Support Libraries</a> (Apache License v2)</li>"+
                                "<li><a href=\"https://github.com/mikepenz/MaterialDrawer\">MaterialDrawer</a> (Apache License v2)</li>"+
                                "<li><a href=\"https://github.com/mikepenz/Android-Iconics\">Android-Iconics</a> (Apache License v2)</li>"+
                            "</ul>" +
                    "</body>" +
                "</html>";

        View view = inflater.inflate(R.layout.about_fragment, container, false);
        WebView webView = view.findViewById(R.id.about_html);
        webView.loadDataWithBaseURL(null, aboutHtml, "text/html", "utf-8", null);

        TextView appVersion = view.findViewById(R.id.about_appversion);
        appVersion.setText("Version: " + BuildConfig.VERSION_NAME + " Code: " + BuildConfig.VERSION_CODE);

        return view;
    }
}
