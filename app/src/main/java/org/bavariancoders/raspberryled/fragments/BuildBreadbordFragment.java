/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.bavariancoders.raspberryled.R;

/**
 * Created by BavarianCoders on 28.09.17
 */

public class BuildBreadbordFragment extends Fragment {

    private static final String url = "http://dordnung.de/raspberrypi-ledstrip/";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.build_breadbord_fragment, container, false);
        setHasOptionsMenu(true);

        WebView webView = view.findViewById(R.id.breadboard_guide);

        final ProgressBar progress = new ProgressBar(getContext());

        webView.getSettings().setJavaScriptEnabled(true);

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                progress.setProgress(newProgress);
            }
        });

        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    Toast.makeText(getActivity(), "Oh no! " + error.getDescription(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (url.equals(String.valueOf(request.getUrl()))) {
                        view.loadUrl(String.valueOf(request.getUrl()));
                    } else {
                        Intent intent = new Intent(Intent.ACTION_VIEW, request.getUrl());
                        if (intent.resolveActivity(getActivity().getPackageManager()) != null)
                            startActivity(intent);
                    }
                }
                return true;
            }

            // for < API 21
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String nextUrl) {
                if (url.equals(nextUrl))
                    view.loadUrl(nextUrl);
                else {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(nextUrl));
                    if (intent.resolveActivity(getActivity().getPackageManager()) != null)
                        startActivity(intent);
                }
                return true;
            }
        });

        webView.loadUrl(url);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.build_breadbord_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.open_in_browser:
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                if (intent.resolveActivity(getActivity().getPackageManager()) != null)
                    startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
