/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import org.bavariancoders.raspberryled.ChangeLog;
import org.bavariancoders.raspberryled.ConstantsFunctions;
import org.bavariancoders.raspberryled.adapter.ButtonClickListener;
import org.bavariancoders.raspberryled.adapter.DeviceRecyclerViewAdapter;
import org.bavariancoders.raspberryled.adapter.ItemClickListener;
import org.bavariancoders.raspberryled.dialogs.ColorPickerDialog;
import org.bavariancoders.raspberryled.model.Container;
import org.bavariancoders.raspberryled.model.Device;
import org.bavariancoders.raspberryled.model.DeviceContainer;
import org.bavariancoders.raspberryled.model.InvalidIPv4Exception;
import org.bavariancoders.raspberryled.model.LinkUnlinkException;
import org.bavariancoders.raspberryled.model.PortFormatException;
import org.bavariancoders.raspberryled.net.LEDCommandSender;
import org.bavariancoders.raspberryled.net.ServerOnlineTask;
import org.bavariancoders.raspberryled.net.interfaces.OnProgressUpdateListener;
import org.bavariancoders.raspberryled.storage.DataStorage;
import org.bavariancoders.raspberryled.storage.DataStorageException;
import org.bavariancoders.raspberryled.storage.DeviceXMLFile;
import org.bavariancoders.raspberryled.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by BavarianCoders on 18.09.17
 */

public class MainFragment extends Fragment implements View.OnClickListener, OnProgressUpdateListener {

    private DeviceRecyclerViewAdapter adapter;

    private RecyclerView deviceList;
    private TextInputLayout ipaddresswrapper;
    private TextInputLayout portwrapper;
    private TextInputLayout namewrapper;
    private FloatingActionButton addClose, addDevice, editDevice;
    private Animation expandTextLayouts, collapseTextLayouts, rotateForward, rotateBackwards;
    private ConstraintLayout textLayouts;
    private SwipeRefreshLayout swipeRefresh;

    private Container<Device> deviceContainer = DeviceContainer.instance();
    private DataStorage<Device> storage;
    private ActionMode actionMode;

    private static int selectedEditItem = 0;
    private boolean isAddDeviceOpen = false;
    private boolean isEditDeviceOpen = false;

    private OnDeviceListListener deviceListListener;
    private View fragmentsView;

    public interface OnDeviceListListener {
        void onDeviceItemSelected (List<Integer> deviceIds);
    }

    @Override
    public void onAttach (Context context) {
        super.onAttach(context);
        Activity activity = null;

        if (context instanceof Activity) {
            activity = (Activity) context;
        }

        try {
            deviceListListener = (OnDeviceListListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnDeviceListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.main_fragment, container, false);
        setHasOptionsMenu(true);

        deviceList = (RecyclerView) view.findViewById(R.id.devicelist);
        ipaddresswrapper = (TextInputLayout) view.findViewById(R.id.ipadresswrapper);
        portwrapper = (TextInputLayout) view.findViewById(R.id.portwrapper);
        namewrapper = (TextInputLayout) view.findViewById(R.id.namewrapper);
        textLayouts = (ConstraintLayout) view.findViewById(R.id.textlayouts);
        swipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.device_list_refresh);

        expandTextLayouts = AnimationUtils.loadAnimation(getContext().getApplicationContext(), R.anim.expand_textlayouts);
        collapseTextLayouts = AnimationUtils.loadAnimation(getContext().getApplicationContext(), R.anim.collapse_textlayouts);
        rotateBackwards = AnimationUtils.loadAnimation(getContext().getApplicationContext(), R.anim.rotate_backwards);
        rotateForward = AnimationUtils.loadAnimation(getContext().getApplicationContext(), R.anim.rotate_forward);

        deviceList.setHasFixedSize(true);
        deviceList.setItemAnimator(new DefaultItemAnimator());

        adapter = new DeviceRecyclerViewAdapter(deviceContainer, getContext());

        storage = DeviceXMLFile.instance("load", this.getContext().getApplicationContext(), "devicelist.xml");
        try {
            storage.loadAll(deviceContainer);
            adapter.notifyDataSetChanged();
        } catch (DataStorageException e) {
            e.printStackTrace();
        }

        adapter.setOnChooseColorClickListener(new ButtonClickListener() {
            @Override
            public void onButtonClick(final int position, final View v) {
                new ColorPickerDialog(MainFragment.this.getActivity(),
                        deviceContainer.getLinkElement(position).getLastColor(), new ColorPickerDialog.OnColorSelectedListener() {
                    @Override
                    public void onColorSelected(int color) {
                        String rgbString = ConstantsFunctions.getJsonRgb(Color.red(color), Color.green(color), Color.blue(color));

                        new LEDCommandSender(rgbString, deviceContainer.getLinkElement(position)).start(1);

                        deviceContainer.getLinkElement(position).setLastColor(color);
                        adapter.notifyItemChanged(position);
                        storage = DeviceXMLFile.instance("save", MainFragment.this.getContext().getApplicationContext(), ConstantsFunctions.DEVICE_XML_PATH);
                        try {
                            storage.saveAll(deviceContainer);
                        } catch (DataStorageException e) {
                            e.printStackTrace();
                        } finally {
                            storage.close();
                        }

                    }
                }).show();
            }
        });

        deviceList.setAdapter(adapter);

        LinearLayoutManager llmanager = new LinearLayoutManager(this.getContext(), LinearLayoutManager.VERTICAL, false);
        deviceList.setLayoutManager(llmanager);
        deviceList.setHasFixedSize(true);

        Device[] deviceArray = deviceListToArray();
        if (deviceArray.length > 0) new ServerOnlineTask(this, deviceArray).start(deviceArray.length);

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Device[] deviceArray = deviceListToArray();

                final List<Future<Object>> futureList =
                        new ServerOnlineTask(MainFragment.this, deviceArray).start(deviceArray.length);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                swipeRefresh.setRefreshing(true);
                            }
                        });
                        for (int i = 0; i < futureList.size(); i++)
                            try {
                                futureList.get(i).get();
                            } catch (InterruptedException | ExecutionException e) {
                                e.printStackTrace();
                            }
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                swipeRefresh.setRefreshing(false);
                            }
                        });
                    }
                }).start();
            }
        });

        ChangeLog cl = new ChangeLog(this.getContext());
        if (cl.firstRun())
            cl.getLogDialog().show();

        return view;
    }

    @Override
    public void onViewCreated (View view, Bundle savedInstanceState) {
        fragmentsView = view;

        addClose = (FloatingActionButton) fragmentsView.findViewById(R.id.add_close);
        addDevice = (FloatingActionButton) fragmentsView.findViewById(R.id.add_device);
        editDevice = (FloatingActionButton) fragmentsView.findViewById(R.id.edit_confirm);

        addDevice.setOnClickListener(this);
        addClose.setOnClickListener(this);
        editDevice.setOnClickListener(this);

        adapter.setOnItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                if (actionMode != null) {
                    adapter.toggleSelection(position);
                    if (adapter.getSelectedItemCount() == 0) {
                        actionMode.finish();
                        return;
                    }
                    String title = getString(R.string.selected_count, adapter.getSelectedItemCount());
                    actionMode.setTitle(title);
                    actionMode.invalidate();
                    return;
                }
                List<Integer> deviceIds = new ArrayList<Integer>();
                deviceIds.add(position);
                deviceListListener.onDeviceItemSelected(deviceIds);
            }

            @Override
            public void onItemLongClick(int position, View v) {
                if (actionMode != null) return;

                actionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(new MainFragment.SelectionCallbackHandler());

                adapter.toggleSelection(position);
                String title = getString(R.string.selected_count, adapter.getSelectedItemCount());
                actionMode.setTitle(title);
                actionMode.invalidate();
                v.setActivated(true);
            }
        });
    }

    @Override
    public void onProgressUpdate(Device... params) {
        for (int i = 0; i < deviceContainer.getSize(); i++)
            if (deviceContainer.getLinkElement(i).equals(params[0]))
                adapter.notifyItemChanged(i);
    }

    @Override
    public void onClick (View view) {
        switch (view.getId()) {
            case R.id.add_device:
                addDevice();
                break;
            case R.id.edit_confirm:
                editDevice();
                break;
            case R.id.add_close:
                addClose();
                break;
        }
    }

    private Device[] deviceListToArray() {
        Device[] deviceArray = new Device[deviceContainer.getSize()];
        for (int i = 0; i < deviceContainer.getSize(); i++)
            deviceArray[i] = deviceContainer.getLinkElement(i);
        return deviceArray;
    }

    private void addDevice() {
        String name = namewrapper.getEditText().getText().toString();
        String ipaddress = ipaddresswrapper.getEditText().getText().toString();
        String port = portwrapper.getEditText().getText().toString();
        clearTextInputError();

        if(ipaddress.length() == 0) {
            ipaddresswrapper.setError(getString(R.string.mandatory_field));
            return;
        }
        if (port.length() == 0) {
            portwrapper.setError(getString(R.string.mandatory_field));
            return;
        }
        try {
            ipaddresswrapper.setErrorEnabled(false);
            portwrapper.setErrorEnabled(false);
            int intport = Integer.valueOf(port);
            deviceContainer.linkElement(new Device(ipaddress, intport, name, Color.BLACK));
            adapter.notifyItemInserted(adapter.getItemCount() - 1);
            Toast.makeText(MainFragment.this.getContext(), "Succesfully inserted in list", Toast.LENGTH_SHORT).show();
            animateTextLayouts();
            clearTextInputLayouts();
            storage = DeviceXMLFile.instance("save", this.getContext().getApplicationContext(), "devicelist.xml");
            storage.saveAll(deviceContainer);

        } catch (LinkUnlinkException e) {
            Toast.makeText(MainFragment.this.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();

        } catch (InvalidIPv4Exception e) {
            ipaddresswrapper.setError(e.getMessage());

        } catch (PortFormatException e) {
            portwrapper.setError(e.getMessage());

        } catch (NumberFormatException e) {
            portwrapper.setError("Number has wrong format. " + e.getMessage());

        } catch (DataStorageException e) {
            e.printStackTrace();
        }
        finally {
            storage.close();
        }

    }

    private void editDevice() {
        String name = namewrapper.getEditText().getText().toString();
        String ipaddress = ipaddresswrapper.getEditText().getText().toString();
        String port = portwrapper.getEditText().getText().toString();
        Device editedDevice = null;

        try {
            ipaddresswrapper.setErrorEnabled(false);
            portwrapper.setErrorEnabled(false);
            int intport = Integer.valueOf(port);
            editedDevice = deviceContainer.getLinkElement(selectedEditItem);
            int color = editedDevice.getLastColor();

            deviceContainer.replaceElement(selectedEditItem, new Device(ipaddress, intport, name, color));
            adapter.notifyItemChanged(selectedEditItem);
            storage = DeviceXMLFile.instance("save", this.getContext().getApplicationContext(), ConstantsFunctions.DEVICE_XML_PATH);
            storage.saveAll(deviceContainer);

        } catch (LinkUnlinkException e) {
            Toast.makeText(MainFragment.this.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            try {
                deviceContainer.linkElement(editedDevice);
            } catch (LinkUnlinkException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
            return;

        } catch (InvalidIPv4Exception e) {
            ipaddresswrapper.setError(e.getMessage());
            try {
                deviceContainer.linkElement(editedDevice);
            } catch (LinkUnlinkException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
            return;

        } catch (PortFormatException e) {
            portwrapper.setError(e.getMessage());
            try {
                deviceContainer.linkElement(editedDevice);
            } catch (LinkUnlinkException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
            return;

        } catch (NumberFormatException e) {
            portwrapper.setError("Number has wrong format. " + e.getMessage());
            try {
                deviceContainer.linkElement(editedDevice);
            } catch (LinkUnlinkException e1) {
                e1.printStackTrace();
            }
            return;
        } catch (DataStorageException e) {
            e.printStackTrace();
        } finally {
            storage.close();
        }

        editedDevice = null;
        Toast.makeText(MainFragment.this.getContext(), "Succesfully edited Device", Toast.LENGTH_SHORT).show();
        clearTextInputLayouts();
        animateEdit();
        actionMode.finish();
    }

    private void addClose () {
        if (isEditDeviceOpen) {
            animateEdit();
            clearTextInputLayouts();
            return;
        }
        animateTextLayouts();
    }

    @Override
    public void onCreateOptionsMenu (Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.toolbar_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        Device[] devices = new Device[deviceContainer.getSize()];
        for (int i = 0; i < deviceContainer.getSize(); i++)
            devices[i] = deviceContainer.getLinkElement(i);
        switch (item.getItemId()) {
            case R.id.all_on:
                LEDCommandSender sender = new LEDCommandSender("turnOn", devices);
                ArrayList<Future<Object>> list = (ArrayList<Future<Object>>) sender.start(deviceContainer.getSize());
                for ( Future<Object> f : list )
                    try {
                        f.get();
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                    }

                return true;

            case R.id.all_off:
                LEDCommandSender sender1 = new LEDCommandSender("turnOff", devices);
                ArrayList<Future<Object>> list1 = (ArrayList<Future<Object>>) sender1.start(deviceContainer.getSize());
                for ( Future<Object> f : list1 )
                    try {
                        f.get();
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                    }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class SelectionCallbackHandler implements ActionMode.Callback {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.selection_toolbar, menu);
            swipeRefresh.setEnabled(false);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            MenuItem editItem = menu.findItem(R.id.edit_item);
            editItem.setVisible(false);
            if (adapter.getSelectedItemCount() == 1)
                editItem.setVisible(true);
            return true;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

            switch (item.getItemId()) {
                case R.id.led_action:
                    deviceListListener.onDeviceItemSelected(adapter.getSelectedItems());
                    actionMode.finish();
                    break;

                case R.id.delete_items:
                    ArrayList<Integer> delList = (ArrayList<Integer>) adapter.getSelectedItems();
                    Collections.sort(delList, Collections.<Integer>reverseOrder());
                    for (int i : delList)
                        try {
                            deviceContainer.unlinkElement(deviceContainer.getLinkElement(i));
                            adapter.notifyItemRemoved(i);

                        } catch (LinkUnlinkException e) {
                            Toast.makeText(MainFragment.this.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    try {
                        storage = DeviceXMLFile.instance("save", MainFragment.this.getContext().getApplicationContext(), ConstantsFunctions.DEVICE_XML_PATH);
                        storage.saveAll(deviceContainer);
                    } catch (DataStorageException e) {
                        e.printStackTrace();
                    }
                    finally {
                        storage.close();
                    }
                    actionMode.finish();
                    break;

                case R.id.edit_item:
                    ArrayList<Integer> sel = (ArrayList<Integer>) adapter.getSelectedItems();
                    Device d = deviceContainer.getLinkElement(sel.get(0));
                    selectedEditItem = sel.get(0);
                    ipaddresswrapper.getEditText().setText(d.getIP());
                    portwrapper.getEditText().setText(Integer.toString(d.getPort()));
                    namewrapper.getEditText().setText(d.getName());
                    if (isEditDeviceOpen)
                        clearTextInputLayouts();
                    animateEdit();
                    break;
            }

            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            actionMode = null;
            adapter.clearSelection();
            clearTextInputLayouts();
            if (isEditDeviceOpen)
                animateEdit();
            swipeRefresh.setEnabled(true);
        }
    }

    private void animateTextLayouts () {
        if (isAddDeviceOpen) {
            addClose.startAnimation(rotateBackwards);
            addDevice.animate().translationX(0f).setInterpolator(new AccelerateDecelerateInterpolator());
            addDevice.setClickable(false);
            textLayouts.startAnimation(collapseTextLayouts);
            for (int i = 0; i < textLayouts.getChildCount(); i++)
                textLayouts.getChildAt(i).setVisibility(View.INVISIBLE);
            isAddDeviceOpen = false;
            View view = this.getActivity().getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager)this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            return;
        }

        for (int i = 0; i < textLayouts.getChildCount(); i++)
            textLayouts.getChildAt(i).setVisibility(View.VISIBLE);
        addClose.startAnimation(rotateForward);
        textLayouts.startAnimation(expandTextLayouts);
        addDevice.animate().translationX(-(float) fragmentsView.getWidth()/2f +
                (float)addDevice.getWidth()/2f + fragmentsView.getWidth() - addDevice.getX() -
                addDevice.getWidth()).setInterpolator(new AccelerateDecelerateInterpolator());
        addDevice.setClickable(true);
        isAddDeviceOpen = true;

    }

    private void animateEdit () {
        if (isAddDeviceOpen)
            animateTextLayouts();
        if (isEditDeviceOpen) {
            addClose.startAnimation(rotateBackwards);
            editDevice.animate().translationX(0f).setInterpolator(new AccelerateDecelerateInterpolator());
            editDevice.setClickable(false);
            textLayouts.startAnimation(collapseTextLayouts);
            for (int i = 0; i < textLayouts.getChildCount(); i++)
                textLayouts.getChildAt(i).setVisibility(View.INVISIBLE);
            isEditDeviceOpen = false;
            View view = this.getActivity().getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager)this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            return;
        }

        for (int i = 0; i < textLayouts.getChildCount(); i++)
            textLayouts.getChildAt(i).setVisibility(View.VISIBLE);
        addClose.startAnimation(rotateForward);
        textLayouts.startAnimation(expandTextLayouts);
        editDevice.animate().translationX(-(float) this.getView().getWidth()/2f +
                (float)editDevice.getWidth()/2f + this.getView().getWidth() - editDevice.getX() -
                editDevice.getWidth()).setInterpolator(new AccelerateDecelerateInterpolator());
        editDevice.setClickable(true);
        isEditDeviceOpen = true;

    }

    private void clearTextInputLayouts () {
        ipaddresswrapper.getEditText().setText("");
        portwrapper.getEditText().setText("");
        namewrapper.getEditText().setText("");
    }

    private void clearTextInputError () {
        ipaddresswrapper.setErrorEnabled(false);
        portwrapper.setErrorEnabled(false);
        namewrapper.setErrorEnabled(false);
    }
}
