/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.model;

import android.graphics.Color;

import org.bavariancoders.raspberryled.ConstantsFunctions;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by BavarianCoders on 07.03.17
 */

public class InterpolateElem extends ScriptElem {
    private int start_color;
    private int end_color;
    private int steps;
    private int time; //time in s

    public InterpolateElem(int start_color, int end_color, int steps, int time){
        super.get_id();
        set_start_color(start_color);
        set_end_color(end_color);
        set_steps(steps);
        set_time(time);
    }

    public void set_start_color(int start_color){
        this.start_color = start_color;
    }

    public void set_end_color(int end_color){
        this.end_color = end_color;
    }

    public void set_steps(int steps){
        this.steps = steps;
    }

    public void set_time(int time){
        this.time = time;
    }

    public int get_start_color(){
        return start_color;
    }

    public int get_end_color(){
        return end_color;
    }

    public int get_steps(){
        return steps;
    }

    public int get_time(){
        return time;
    }

    @Override
    public String toString(){
        String firstColor =String.format("%03d%03d%03d", Color.red(start_color), Color.green(start_color), Color.blue(start_color));
        String secondColor =String.format("%03d%03d%03d", Color.red(end_color), Color.green(end_color), Color.blue(end_color));
        String steps = String.format("%04d",this.steps);
        String time = String.format("%02d", this.time);
        return "2"+ firstColor+ secondColor + steps + time;
    }

    @Override
    public String kindtoText(){
        return ("Inter");
    }

    @Override
    public String paramtoText(){
        return ("("+android.graphics.Color.red(start_color)+"|"+android.graphics.Color.green(start_color)+"|"+android.graphics.Color.blue(start_color)+")"+steps+"|"+time+"\n("+android.graphics.Color.red(end_color)+"|"+android.graphics.Color.green(end_color)+"|"+android.graphics.Color.blue(end_color)+")");
    }

    @Override
    public int[] getInfo(){
        int[] i = {this.start_color, this.end_color, this.time, this.steps};
        return i;
    }

    @Override
    protected JSONObject toJSONObject() {
        try {
            JSONObject jsonObject = new JSONObject(ConstantsFunctions.INTERPOLATEELEM_OBJECT);
            JSONObject transitionObject = jsonObject.getJSONObject(ConstantsFunctions.INTERPOLATE_VALUE);
            JSONObject startObject = transitionObject.getJSONObject(ConstantsFunctions.STARTCOLOR_OBJECT);
            JSONObject endObject = transitionObject.getJSONObject(ConstantsFunctions.ENDCOLOR_OBJECT);
            startObject.put(ConstantsFunctions.VALUE_RED, Color.red(start_color));
            startObject.put(ConstantsFunctions.VALUE_GREEN, Color.green(start_color));
            startObject.put(ConstantsFunctions.VALUE_BLUE, Color.blue(start_color));
            endObject.put(ConstantsFunctions.VALUE_RED, Color.red(end_color));
            endObject.put(ConstantsFunctions.VALUE_GREEN, Color.green(end_color));
            endObject.put(ConstantsFunctions.VALUE_BLUE, Color.blue(end_color));
            transitionObject.put(ConstantsFunctions.VALUE_TIME, time);
            transitionObject.put(ConstantsFunctions.VALUE_STEPS, steps);
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
