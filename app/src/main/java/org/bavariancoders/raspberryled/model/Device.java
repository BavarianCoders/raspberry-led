/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.model;

import java.net.Socket;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by BavarianCoders on 04.03.17
 */

public class Device {

    private static final int MIN_PORT = 0;
    private static final int MAX_PORT = 65535;
    private static final String IP4_PATTERN = "\\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.)" +
            "{3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\b";

    private String IP;
    private String name = null;
    private int port;
    private boolean isReachable = false;

    private static Socket socket;
    private int lastColor;

    public Device (String IP, int port, int color) throws PortFormatException, InvalidIPv4Exception{
        setIP(IP);
        setPort(port);
        setLastColor(color);
    }

    public Device (String IP, int port, String name, int color) throws PortFormatException, InvalidIPv4Exception {
        setIP(IP);
        setPort(port);
        setName(name);
        setLastColor(color);
    }

    public String getIP() {
        return IP;
    }

    public int getPort () {
        return port;
    }

    public String getName() { return name; }

    public boolean getIsReachable() { return isReachable; }

    public void setIsReachable(boolean isReachable) { this.isReachable = isReachable; }

    public void setIP (String IP) throws InvalidIPv4Exception{
        Pattern pattern = Pattern.compile(IP4_PATTERN);
        Matcher matcher = pattern.matcher(IP);
        if (!matcher.matches())
            throw new InvalidIPv4Exception (null, "Invalid IPv4");
        this.IP = IP;
    }

    public void setPort (int port) throws PortFormatException {
        if (port < MIN_PORT)
            throw new PortFormatException(null, "port can not be negative");
        if (port > MAX_PORT)
            throw new PortFormatException(null, "port doesn't exist. Choose a number between: " +
                    MIN_PORT + " and " + MAX_PORT);

        this.port = port;
    }

    public void setName (String newName) {
        this.name = newName;
    }

    public void setLastColor (int color) {
        this.lastColor = color;
    }

    public int getLastColor () {
        return this.lastColor;
    }

    @Override
    public boolean equals (Object o) {
        if (o == this)
            return true;
        if (o == null)
            return false;
        if (!o.getClass().equals(this.getClass()))
            return false;
        Device d = (Device) o;
        if (name != null && d.getName() != null)
            return IP.compareTo(d.getIP()) == 0 && port == d.getPort() && name.compareTo(d.getName()) == 0;

        return IP.compareTo(d.getIP()) == 0 && port == d.getPort();
    }

    @Override
    public String toString () {
        if (name != null)
            return this.name + "\n" + this.IP + ":" + this.port;
        return this.IP + ":" + this.port;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        Device.socket = socket;
    }
}
