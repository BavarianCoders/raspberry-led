/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by BavarianCoders on 22.08.17
 */

public class ScriptListContainer implements Container<ScriptContainer>{
    private List<ScriptContainer> dataSet = new ArrayList<ScriptContainer>();
    private static ScriptListContainer unique;

    public static ScriptListContainer instance() {
        if (unique == null) return (unique = new ScriptListContainer());
        return unique;
    }

    private ScriptListContainer(){}

    @Override
    public void linkElement(ScriptContainer element) throws LinkUnlinkException {
        if (element == null)
            throw new LinkUnlinkException(null, "Given element is null!");
        if (dataSet.contains(element))
            throw new LinkUnlinkException(null, "Given ScriptContainer is already in the List: " + element.getName() + "(" + element.getID() + ")");
        if (!dataSet.add(element))
            throw new LinkUnlinkException(null, "Error while adding the element: " + element.getName() + "(" + element.getID() + ")");
    }

    @Override
    public void unlinkElement(ScriptContainer element) throws LinkUnlinkException {
        if (element == null) throw new LinkUnlinkException(null, "Given element is null");
        if (!dataSet.remove(element)) throw new LinkUnlinkException(null, "Sorry could not remove element");
    }

    @Override
    public void replaceElement(int pos, ScriptContainer element) throws LinkUnlinkException {
        if (pos < 0) throw new LinkUnlinkException(null, "pos is negative");
        if (element == null) throw new LinkUnlinkException(null, "Given element is null");
        dataSet.set(pos, element);
    }

    @Override
    public ScriptContainer getLinkElement(int pos) {
        if (pos < 0) return null;
        return dataSet.get(pos);
    }

    @Override
    public int getSize() {
        return dataSet.size();
    }

    @Override
    public void clearContainer() {
        dataSet.clear();
    }

    @Override
    public Iterator<ScriptContainer> iterator() {
        return dataSet.iterator();
    }
}
