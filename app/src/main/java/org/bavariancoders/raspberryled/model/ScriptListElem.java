/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.model;

/**
 * Created by BavarianCoders on 19.03.17
 */

public class ScriptListElem {
    private int id;
    private String name;
    private int amount;

    public ScriptListElem(String name, int amount, int id){
        this.id = id;
        this.name = name;
        this.amount = amount;
    }

    @Override
    public String toString(){
        return name +" ("+amount+")";
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return this.name;
    }

    public int getAmount () {
        return this.amount;
    }

    public void setId (int id) {
        this.id = id;
    }

    public void setName (String name) {
        this.name = name;
    }

    public void setAmount (int amount) {
        this.amount = amount;
    }
}
