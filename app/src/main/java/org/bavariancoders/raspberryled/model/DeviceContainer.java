/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by BavarianCoders on 04.03.17
 */

public class DeviceContainer implements Container<Device> {
    private List<Device> dataSet = new ArrayList<Device>();
    private static DeviceContainer unique;

    private DeviceContainer() {}

    public static DeviceContainer instance() {
        if (unique == null)
            return (unique = new DeviceContainer());

        return unique;
    }

    @Override
    public void linkElement(Device newDevice) throws LinkUnlinkException {
        if (newDevice == null)
            throw new LinkUnlinkException(null, "given Device is null");
        if (dataSet.contains(newDevice))
            throw new LinkUnlinkException(null, "given Device is already in the List");
        if (!dataSet.add(newDevice))
            throw new LinkUnlinkException(null, "error while inserting the Device: " + newDevice.toString());
    }

    @Override
    public void unlinkElement(Device delDevice) throws LinkUnlinkException {
        if (delDevice == null)
            throw new LinkUnlinkException(null, "given Device is null");

        if(!dataSet.remove(delDevice))
            throw new LinkUnlinkException(null, "Error deleting the Device from the list");
    }

    @Override
    public void replaceElement(int pos, Device element) throws LinkUnlinkException {
        if (element == null)
            throw new LinkUnlinkException(null, "Given device is null.");
        if (pos < 0 || pos >= getSize())
            throw new LinkUnlinkException(null, "Index is out of bounds of the Array. Can not " +
                    "replace the device. Size of array: " + getSize() + " Index: " + pos);
        dataSet.set(pos, element);
    }

    @Override
    public Device getLinkElement(int pos) {
        return dataSet.get(pos);
    }

    @Override
    public int getSize() {
        return dataSet.size();
    }

    @Override
    public void clearContainer() {
        dataSet.clear();
    }

    @Override
    public Iterator<Device> iterator() {
        return dataSet.iterator();
    }
}
