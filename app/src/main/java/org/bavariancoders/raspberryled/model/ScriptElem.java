/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.model;

import org.json.JSONObject;

/**
 * Created by BavarianCoders on 07.03.17
 */

public abstract class ScriptElem {
    private static int id = -1;
    private boolean swapSet = false;
    public void give_id(){
        if(id == -1)
            id++;
    }

    public boolean getSwapSet () {
        return swapSet;
    }

    public void setSwapSet (boolean swapSet) {
        this.swapSet = swapSet;
    }

    public int get_id(){
        return id;
    }

    @Override
    public abstract String toString();

    public abstract String kindtoText();

    public abstract String paramtoText();

    public abstract int[] getInfo();

    protected abstract JSONObject toJSONObject();

}
