/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.model;

import android.graphics.Color;

import org.bavariancoders.raspberryled.ConstantsFunctions;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by BavarianCoders on 07.03.17
 */

public class SetElem extends ScriptElem {
    int color;
    public SetElem(int color){
        super.give_id();
        set_color(color);
    }

    public void set_color(int color){
        this.color = color;
    }

    public int get_color(){
        return color;
    }

    @Override
    public String toString(){
        String part = String.format("%03d%03d%03d", Color.red(color), Color.green(color), Color.blue(color));
       return ("0"+part);

    }

    @Override
    public String kindtoText(){
        return ("Set");
    }

    @Override
    public String paramtoText(){
        return ("("+android.graphics.Color.red(color)+"|"+android.graphics.Color.green(color)+"|"+android.graphics.Color.blue(color)+")");
    }

    @Override
    public int[] getInfo(){
        int[] i = {this.color};
        return i;
    }

    @Override
    protected JSONObject toJSONObject() {
        try {
            JSONObject jsonObject = new JSONObject(ConstantsFunctions.SETELEM_OBJECT);
            JSONObject setObject = jsonObject.getJSONObject(ConstantsFunctions.SET_VALUE);
            setObject.put(ConstantsFunctions.VALUE_RED, Color.red(color));
            setObject.put(ConstantsFunctions.VALUE_GREEN, Color.green(color));
            setObject.put(ConstantsFunctions.VALUE_BLUE, Color.blue(color));
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
}
