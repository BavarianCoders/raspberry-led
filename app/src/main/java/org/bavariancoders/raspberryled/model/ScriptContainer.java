/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.model;

import org.bavariancoders.raspberryled.ConstantsFunctions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by BavarianCoders on 07.03.17
 */

public class ScriptContainer implements Container<ScriptElem>{
    private int id = 0;
    private String name;
    private ArrayList<ScriptElem> mDataset = null;

    public ScriptContainer(int id, String name){
        this.id = id;
        this.name = name;
        mDataset = new ArrayList<ScriptElem>();
    }

    public boolean add(ScriptElem new_elem){
        return mDataset.add(new_elem);
    }

    public boolean remove(ScriptElem delete_elem){
        if (mDataset.contains(delete_elem))
            return mDataset.remove(delete_elem);
        return true;
    }

    @Override
    public ScriptElem getLinkElement(int pos){
        return mDataset.get(pos);
    }

    public ScriptElem get_by_id(int id){
        for (ScriptElem elem : mDataset){
            if (elem.get_id() == id)
                return elem;
        }
        return null;
    }

    public List<ScriptElem> getScriptList () {
        return mDataset;
    }

    public int size(){
        return mDataset.size();
    }

    public String getSignal() {
        String Signal ="999";
        for (ScriptElem elem : mDataset){
            Signal = Signal + elem.toString();
        }

        return Signal+"E";
    }

    public String getJSONSignal () {
        try {
            JSONObject jsonObject = new JSONObject();
            JSONArray effectArray = new JSONArray();
            for (ScriptElem elem : mDataset) {
                effectArray.put(elem.toJSONObject());
            }
            jsonObject.put(ConstantsFunctions.LIGHTEFFECT_ARRAY, effectArray);
            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void restoreswap(){
        for(ScriptElem elem: mDataset){
            elem.setSwapSet(false);
        }
    }



    public int getNewId(){
        return id++;
    }

    @Override
    public void linkElement(ScriptElem element) throws LinkUnlinkException {
        if (element == null)
            throw new LinkUnlinkException(null, "given element is null");
        if (!mDataset.add(element))
            throw new LinkUnlinkException(null, "Element could not added to list");

    }

    @Override
    public void unlinkElement(ScriptElem element) throws LinkUnlinkException {
        if (element == null)
            throw new LinkUnlinkException(null, "Given element is null");
        if (!mDataset.remove(element))
            throw new LinkUnlinkException(null, "Error while removing the item from list");
    }

    @Override
    public void replaceElement(int pos, ScriptElem element) throws LinkUnlinkException {
        if (element == null)
            throw new LinkUnlinkException(null, "Given device is null");
        if (pos < 0 || pos >= getSize())
            throw new LinkUnlinkException(null, "Index out of bounds of the array. Can not " +
                    "replace the script element. Size of array: " + getSize() + " Index: " + pos);
        mDataset.set(pos, element);
    }

    @Override
    public int getSize() {
        return mDataset.size();
    }

    @Override
    public void clearContainer() {
        mDataset.clear();
    }

    @Override
    public Iterator<ScriptElem> iterator() {
        return mDataset.iterator();
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (o == null)
            return false;
        if (!o.getClass().equals(this.getClass()))
            return false;
        ScriptContainer c = (ScriptContainer) o;
        return (this.id == c.getID()) && (this.name.compareTo(c.getName()) == 0);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getID () {return id;}

    public void setID (int newID) {this.id = newID;}
}
