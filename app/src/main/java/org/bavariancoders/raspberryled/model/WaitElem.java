/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.model;

import org.bavariancoders.raspberryled.ConstantsFunctions;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by BavarianCoders on 07.03.17
 */

public class WaitElem extends ScriptElem {
    private int time; // time in ms

    public WaitElem(int time){
        super.give_id();
        set_time(time);
    }

    public void set_time(int time){
        this.time = time;
    }

    public int get_Time(){
        return this.time;
    }

    @Override
    public String toString(){
        return ("1"+String.format("%08d",time));
    }

    @Override
    public String kindtoText(){
        return ("Wait");
    }

    @Override
    public String paramtoText(){
        return ("("+Integer.toString(time)+")");
    }

    @Override
    public int[] getInfo(){
        int[] i = {time};
        return i;
    }

    @Override
    protected JSONObject toJSONObject() {
        try {
            JSONObject jsonObject = new JSONObject(ConstantsFunctions.WAITELEM_OBJECT);
            JSONObject waitObject = jsonObject.getJSONObject(ConstantsFunctions.WAIT_VALUE);
            waitObject.put(ConstantsFunctions.VALUE_TIME, (double)time / 10000d);
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
