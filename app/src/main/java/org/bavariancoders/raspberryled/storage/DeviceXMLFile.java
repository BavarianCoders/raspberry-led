/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.storage;

import android.content.Context;
import android.util.Xml;

import org.bavariancoders.raspberryled.ConstantsFunctions;
import org.bavariancoders.raspberryled.model.Container;
import org.bavariancoders.raspberryled.model.DeviceContainer;
import org.bavariancoders.raspberryled.model.Device;
import org.bavariancoders.raspberryled.model.InvalidIPv4Exception;
import org.bavariancoders.raspberryled.model.LinkUnlinkException;
import org.bavariancoders.raspberryled.model.PortFormatException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;

/**
 * Created by BavarianCoders on 17.03.17
 */

public class DeviceXMLFile implements DataStorage<Device>{

    private FileOutputStream fos;
    private StringWriter writer;
    private BufferedReader reader;

    private XmlSerializer serializer;
    private XmlPullParser parser;

    private DeviceXMLFile () {

    }

    public static DataStorage<Device> instance (String mode, Context context, String dataSource) {
        DeviceXMLFile t = null;
        if (mode.compareTo("load") == 0) {
            t = new DeviceXMLFile();
            t.parser = Xml.newPullParser();
            try {
                t.reader = new BufferedReader(new InputStreamReader(new FileInputStream(context.getFilesDir() + "/" + dataSource)));
                t.parser.setInput(t.reader);
                t.parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);

            } catch (IOException | XmlPullParserException e) {
                e.printStackTrace();
            }
        }
        else if (mode.compareTo("save") == 0) {
            t = new DeviceXMLFile();
            t.serializer = Xml.newSerializer();
            try {
                t.fos = new FileOutputStream(context.getFilesDir() + "/" + dataSource, false); // Creates a new File if it doesn't exist else the File will be overwritten
                t.writer = new StringWriter();
                t.serializer.setOutput(t.writer);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return t;
    }

    @Override
    public void loadAll(Container<Device> container) throws DataStorageException {
        try {
            String ip = null, name = null;
            int port = 0, lastColor = 0;
            while (parser.next() != XmlPullParser.END_DOCUMENT) {
                parser.require(XmlPullParser.START_TAG, null, ConstantsFunctions.DEVICE_TAG);
                while (parser.next() != XmlPullParser.END_TAG) {
                    if (parser.getEventType() != XmlPullParser.START_TAG) continue;

                    String currentTag = parser.getName();

                    if (currentTag.compareTo(ConstantsFunctions.IP_TAG) == 0)
                        ip = readIP(parser);
                    else if (currentTag.compareTo(ConstantsFunctions.PORT_TAG) == 0)
                        port = readPort(parser);
                    else if (currentTag.compareTo(ConstantsFunctions.NAME_TAG) == 0)
                        name = readName(parser);
                    else if (currentTag.compareTo(ConstantsFunctions.LASTCOLOR_TAG) == 0)
                        lastColor = readLastColor(parser);
                }
                try {
                    container.linkElement(new Device(ip, port, name, lastColor));
                } catch (LinkUnlinkException | InvalidIPv4Exception | PortFormatException e) {

                    e.printStackTrace();

                }
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
            throw new DataStorageException("Error while reading the Xml file", e.getCause());
        } catch (IOException e) {
            throw new DataStorageException("Could not open Xml file", e.getCause());
        }
    }

    @Override
    public Device load(Container container, int id) throws DataStorageException {
        return null;
    }

    @Override
    public void save (Device device) throws DataStorageException {
        // nothing to do here
    }

    @Override
    public void saveAll(Container<Device> container) throws DataStorageException {
        int i = 0;
        DeviceContainer con = (DeviceContainer) container;
        try {
            writer.flush();
            writer.getBuffer().setLength(0);
            fos.flush();
            serializer.startDocument("UTF-8", true);
        } catch (IOException e) {
            throw new DataStorageException("Could not start the Xml file", e.getCause());
        }
        for (Device device : con) {
            try {
                serializer.startTag(null, ConstantsFunctions.DEVICE_TAG);
                serializer.attribute(null, "id", Integer.toString(i));
                serializer.startTag(null, ConstantsFunctions.IP_TAG);
                serializer.text(device.getIP());
                serializer.endTag(null, ConstantsFunctions.IP_TAG);
                serializer.startTag(null, ConstantsFunctions.PORT_TAG);
                serializer.text(Integer.toString(device.getPort()));
                serializer.endTag(null, ConstantsFunctions.PORT_TAG);
                serializer.startTag(null, ConstantsFunctions.NAME_TAG);
                if (device.getName() != null)
                    serializer.text(device.getName());
                serializer.endTag(null, ConstantsFunctions.NAME_TAG);
                serializer.startTag(null, ConstantsFunctions.LASTCOLOR_TAG);
                serializer.text(Integer.toString(device.getLastColor()));
                serializer.endTag(null, ConstantsFunctions.LASTCOLOR_TAG);
                serializer.endTag(null, ConstantsFunctions.DEVICE_TAG);
            } catch (IOException e) {
                throw new DataStorageException("Could not write xml File", e.getCause());
            }
            i++;
        }
        try {
            serializer.endDocument();
            serializer.flush();
            fos.write(writer.toString().getBytes());
            writer.flush();
            writer.getBuffer().setLength(0);
            fos.flush();
        } catch (IOException e) {
            throw new DataStorageException("Could not end the xml file", e.getCause());
        }

    }

    @Override
    public void modify(Device newT) throws DataStorageException {
        // unused
    }

    @Override
    public void add(Device newS) throws DataStorageException {

    }

    @Override
    public void delete(Device delT) throws DataStorageException {

    }

    @Override
    public void close() {
        try {
            if (fos != null)
                fos.close();
            if (writer != null)
                writer.close();
            if (reader != null)
                reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String readIP (XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, ConstantsFunctions.IP_TAG);
        String ip = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, ConstantsFunctions.IP_TAG);
        return ip;
    }

    private int readPort (XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, ConstantsFunctions.PORT_TAG);
        int port = Integer.valueOf(readText(parser));
        parser.require(XmlPullParser.END_TAG, null, ConstantsFunctions.PORT_TAG);
        return port;
    }
    
    private String readName (XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, ConstantsFunctions.NAME_TAG);
        String name = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, ConstantsFunctions.NAME_TAG);
        return name;
    }

    private int readLastColor (XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, ConstantsFunctions.LASTCOLOR_TAG);
        int lastcolor = Integer.valueOf(readText(parser));
        parser.require(XmlPullParser.END_TAG, null, ConstantsFunctions.LASTCOLOR_TAG);
        return lastcolor;
    }

    private String readText (XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = null;
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }
}
