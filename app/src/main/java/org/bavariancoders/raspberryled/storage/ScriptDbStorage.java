/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.storage;

        import android.content.ContentValues;
        import android.database.sqlite.*;
        import android.content.Context;
        import android.database.Cursor;
        import android.util.Log;

        import org.bavariancoders.raspberryled.model.Container;
        import org.bavariancoders.raspberryled.model.InterpolateElem;
        import org.bavariancoders.raspberryled.model.LinkUnlinkException;
        import org.bavariancoders.raspberryled.model.ScriptContainer;
        import org.bavariancoders.raspberryled.model.ScriptElem;
        import org.bavariancoders.raspberryled.model.ScriptListElem;
        import org.bavariancoders.raspberryled.model.SetElem;
        import org.bavariancoders.raspberryled.model.WaitElem;


        import java.util.ArrayList;

/**
 * Created by BavarianCoders on 07.03.17
 */

public class ScriptDbStorage implements DataStorage<ScriptContainer>{

    private final String SCRIPT_COMMAND = "MainActivity.SCRIPT_COMMAND";

    public static final String SCRIPTLIST_QUERY = "SELECT * FROM "+ ScriptDbHelper.SCRIPT_TABLE;
    private static final String SCRIPTLIST_NAME = "SELECT * FROM " + ScriptDbHelper.SCRIPT_TABLE + " WHERE " + ScriptDbHelper.SCRIPT_NAME;
    public static String GET_SCRIPT_ELEM_QUERY = "SELECT * FROM "+ ScriptDbHelper.SCRIPT_ELEM_TABLE + " WHERE " + ScriptDbHelper.SCRIPT_ELEM_ID + "=";
    private ArrayList<ScriptListElem> ScriptList = null;
    private static ScriptDbStorage instance = null;
    private static SQLiteDatabase db;

    private ScriptDbStorage(Context context){
        ScriptDbHelper mDbHelper = new ScriptDbHelper(context);
        db = mDbHelper.getWritableDatabase();
        ContentValues RED_INTERPOLATE = new ContentValues();
        RED_INTERPOLATE.put(ScriptDbHelper.SCRIPT_ID, 0);
        RED_INTERPOLATE.put(ScriptDbHelper.SCRIPT_NAME, "New Script");
        RED_INTERPOLATE.put(ScriptDbHelper.SCRIPT_AMOUNT, 0);
        db.insert(ScriptDbHelper.SCRIPT_TABLE, null, RED_INTERPOLATE);
        db = mDbHelper.getReadableDatabase();
        ScriptList = new ArrayList<ScriptListElem>();
    }

    public static ScriptDbStorage getInstance(Context context){
        if (instance == null || !db.isOpen()){

            instance = new ScriptDbStorage(context);}
        return instance;
    }

    private void change (int id, String name, ArrayList<ScriptElem> script) {

    }

    private void add (String name, ArrayList<ScriptElem> script) {

    }

    private ScriptContainer loadScript(int id, String name) {
        Cursor cursor = db.rawQuery(GET_SCRIPT_ELEM_QUERY + id, null);
        ScriptContainer scriptContainer = new ScriptContainer(id, name);

        if (cursor.moveToFirst()) {
            do {
                try {
                    if (cursor.isNull(2)) {
                        scriptContainer.linkElement((ScriptElem) new WaitElem(cursor.getInt(4)));
                    } else if (cursor.isNull(3)) {
                        scriptContainer.linkElement((ScriptElem) new SetElem(cursor.getInt(2)));
                    } else {
                        // 2->colorfrom, 3->colorto, 4->time, 5->steps in db-table
                        scriptContainer.linkElement((ScriptElem) new InterpolateElem(cursor.getInt(2),
                                cursor.getInt(3), cursor.getInt(5), cursor.getInt(4)));
                    }
                } catch (LinkUnlinkException e) {
                  e.printStackTrace();
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        return scriptContainer;
    }

    @Override
    public void loadAll(Container<ScriptContainer> container) throws DataStorageException {
        if (container ==  null) throw new DataStorageException("Given container is null. Cancel loading... " + container.toString(), null);
        Cursor cursor = db.rawQuery(SCRIPTLIST_QUERY, null);

        if (cursor.moveToFirst()) {
            do {
                try {
                    ScriptContainer scriptContainer = loadScript(cursor.getInt(0), cursor.getString(1));
                    container.linkElement(scriptContainer);
                } catch (LinkUnlinkException e) {
                    e.printStackTrace();
                }
            } while (cursor.moveToNext());
        }

        cursor.close();
    }

    @Override
    public ScriptContainer load(Container<ScriptContainer> container, int id) throws DataStorageException {

        return null;
    }

    @Override
    public void save (ScriptContainer scriptContainer) throws DataStorageException {
        if (scriptContainer.getID() == 0) {
            add(scriptContainer);
            return;
        }
        modify(scriptContainer);
    }

    @Override
    public void saveAll(Container<ScriptContainer> container) throws DataStorageException {

    }

    @Override
    public void modify(ScriptContainer newScript) throws DataStorageException {
        ContentValues NEW_SCRIPT = new ContentValues();
        NEW_SCRIPT.put(ScriptDbHelper.SCRIPT_NAME , newScript.getName());
        NEW_SCRIPT.put(ScriptDbHelper.SCRIPT_AMOUNT, newScript.getSize());
        db.update(ScriptDbHelper.SCRIPT_TABLE, NEW_SCRIPT, ScriptDbHelper.SCRIPT_ID + "=" + newScript.getID(), null);
        db.delete(ScriptDbHelper.SCRIPT_ELEM_TABLE, ScriptDbHelper.SCRIPT_ELEM_ID + "=" + newScript.getID(), null);

        for (int i = 0; i < newScript.getSize(); i++){
            Log.d("add new to DB",newScript.getLinkElement(i).toString());
            NEW_SCRIPT = new ContentValues();
            NEW_SCRIPT.put(ScriptDbHelper.SCRIPT_ELEM_ID, newScript.getID());
            NEW_SCRIPT.put(ScriptDbHelper.SCRIPT_ELEM_POSITION, i);
            if(newScript.getLinkElement(i).getClass() == SetElem.class){
                NEW_SCRIPT.put(ScriptDbHelper.SCRIPT_ELEM_COLOR, newScript.getLinkElement(i).getInfo()[0]);
                Log.d("added new Set to DB", newScript.getLinkElement(i).toString());
            } else if (newScript.getLinkElement(i).getClass() == WaitElem.class){
                NEW_SCRIPT.put(ScriptDbHelper.SCRIPT_ELEM_TIME, newScript.getLinkElement(i).getInfo()[0]);
            } else {
                NEW_SCRIPT.put(ScriptDbHelper.SCRIPT_ELEM_COLOR, newScript.getLinkElement(i).getInfo()[0]);
                NEW_SCRIPT.put(ScriptDbHelper.SCRIPT_ELEM_COLOR2, newScript.getLinkElement(i).getInfo()[1]);
                NEW_SCRIPT.put(ScriptDbHelper.SCRIPT_ELEM_TIME, newScript.getLinkElement(i).getInfo()[2]);
                NEW_SCRIPT.put(ScriptDbHelper.SCRIPT_ELEM_STEPS, newScript.getLinkElement(i).getInfo()[3]);
            }
            db.insert(ScriptDbHelper.SCRIPT_ELEM_TABLE, null, NEW_SCRIPT);
        }
        Log.d("add new to DB", "ended"+ newScript.toString());
    }

    @Override
    public void add(ScriptContainer newScript) throws DataStorageException {
        ContentValues NEW_SCRIPT = new ContentValues();
        long id;
        NEW_SCRIPT.put(ScriptDbHelper.SCRIPT_NAME, newScript.getName());
        NEW_SCRIPT.put(ScriptDbHelper.SCRIPT_AMOUNT, newScript.getSize());
        id = db.insert(ScriptDbHelper.SCRIPT_TABLE, null, NEW_SCRIPT); // Funktioniert evtl. nicht
        for (int i = 0; i < newScript.getSize(); i++){
            Log.d("add new to DB", newScript.getLinkElement(i).toString());
            NEW_SCRIPT = new ContentValues();
            NEW_SCRIPT.put(ScriptDbHelper.SCRIPT_ELEM_ID, id);
            NEW_SCRIPT.put(ScriptDbHelper.SCRIPT_ELEM_POSITION, i);
            if(newScript.getLinkElement(i).getClass() == SetElem.class){
                NEW_SCRIPT.put(ScriptDbHelper.SCRIPT_ELEM_COLOR, newScript.getLinkElement(i).getInfo()[0]);
                Log.d("added new Set to DB",newScript.getLinkElement(i).toString());
            } else if (newScript.getLinkElement(i).getClass() == WaitElem.class){
                NEW_SCRIPT.put(ScriptDbHelper.SCRIPT_ELEM_TIME, newScript.getLinkElement(i).getInfo()[0]);
            } else {
                NEW_SCRIPT.put(ScriptDbHelper.SCRIPT_ELEM_COLOR, newScript.getLinkElement(i).getInfo()[0]);
                NEW_SCRIPT.put(ScriptDbHelper.SCRIPT_ELEM_COLOR2, newScript.getLinkElement(i).getInfo()[1]);
                NEW_SCRIPT.put(ScriptDbHelper.SCRIPT_ELEM_TIME, newScript.getLinkElement(i).getInfo()[2]);
                NEW_SCRIPT.put(ScriptDbHelper.SCRIPT_ELEM_STEPS, newScript.getLinkElement(i).getInfo()[3]);
            }
            db.insert(ScriptDbHelper.SCRIPT_ELEM_TABLE, null, NEW_SCRIPT);
        }
        Log.d("add new to DB", "ended "+ newScript.toString());
    }

    @Override
    public void delete (ScriptContainer delContainer) throws DataStorageException {
        int i = db.delete(ScriptDbHelper.SCRIPT_ELEM_TABLE,ScriptDbHelper.SCRIPT_ELEM_ID+"=" + delContainer.getID(), null);
        int j = db.delete(ScriptDbHelper.SCRIPT_TABLE,ScriptDbHelper.SCRIPT_ID+" = " + delContainer.getID(), null);

        if (i == 0 || j == 0) throw new DataStorageException("DB: deletion of element " +
                delContainer.getName() + "(" + delContainer.getID() + ")" + " went wrong", null);
    }

    @Override
    public void close() {
        db.close();
    }

}
