/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.storage;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.content.Context;

/**
 * Created by BavarianCoders on 18.03.17
 */

public class ScriptDbHelper extends SQLiteOpenHelper{

    private static final String LOG_TAG = ScriptDbHelper.class.getSimpleName();

    public static final String DB_NAME = "Script.db";
    public static final int DB_Version = 1;

    public static final String SCRIPT_TABLE = "Scripte";
    public static final String SCRIPT_ID = "ID";
    public static final String SCRIPT_NAME = "name";
    public static final String SCRIPT_AMOUNT = "amount"; //Eventuell überflüssig aber gut für Performance beim Laden (Auflisten der Scripte mit Größe)

    public static final String SQL_CREATE_SCRIPT =
            "CREATE TABLE "+SCRIPT_TABLE+"(" +
                    SCRIPT_ID +" INTEGER PRIMARY KEY AUTOINCREMENT," +
                    SCRIPT_NAME + " VARCHAR(20) NOT NULL," +
                    SCRIPT_AMOUNT + " INTEGER NOT NULL);";

    /*public static final String SCRIPT_ELEM_TABLE = "ScriptElem";
    public static final String SCRIPT_ELEM_ID = "ID";
    public static final String SCRIPT_ELEM_SCRIPT_ID = "scriptID";
    public static final String SCRIPT_ELEM_POSITION = "position";

    public static final String SQL_CREATE_SCRIPT_ELEM =
            "CREATE TABLE " + SCRIPT_ELEM_TABLE + "(" +
                    SCRIPT_ELEM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    SCRIPT_ELEM_SCRIPT_ID + " INTEGER NOT NULL REFERENCES "+SCRIPT_TABLE+"(" + SCRIPT_ID + ")," +
                    SCRIPT_ELEM_POSITION + " INTEGER NOT NULL)";

    public static final String SCRIPT_ELEM_SET_TABLE = "Set";
    public static final String SCRIPT_ELEM_SET_ID = "IDDD";
    public static final String SCRIPT_ELEM_SET_COLOR = "color";
    public static final String SCRIPT_ELEM_SET_FOREIGN_KEY_CONSTRAINT = "ScriptElem_forgein_key_constraint";

    public static final String SQL_CREATE_SCRIPT_ELEM_SET = "CREATE TABLE " + SCRIPT_ELEM_TABLE + "(" +
            SCRIPT_ELEM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            SCRIPT_ELEM_SCRIPT_ID + " INTEGER NOT NULL REFERENCES "+SCRIPT_TABLE+"(" + SCRIPT_ID + ")," +
            SCRIPT_ELEM_POSITION + " INTEGER NOT NULL)";
            /*"CREATE TABLE " + SCRIPT_ELEM_SET_TABLE+ "(" +
                    SCRIPT_ELEM_SET_ID + " INTEGER PRIMARY KEY,"+ //REFERENCES " + SCRIPT_TABLE + "(" + SCRIPT_ID + ")," +
                    SCRIPT_ELEM_SET_COLOR + " INTEGER NOT NULL)";
                    //" CONSTRAINT " + SCRIPT_ELEM_SET_FOREIGN_KEY_CONSTRAINT +" "+
                    //SCRIPT_ELEM_SET_ID + " REFERENCES " + SCRIPT_TABLE + "(" + SCRIPT_ID + "))";

    public static final String SCRIPT_ELEM_WAIT_TABLE = "WAIT";
    public static final String SCRIPT_ELEM_WAIT_ID = "ID";
    public static final String SCRIPT_ELEM_WAIT_TIME = "time";
    public static final String SCRIPT_ELEM_WAIT_FOREIGN_KEY_CONSTRAINT = "ScriptElem_forgein_key_constraint";

    public static final String SQL_CREATE_SCRIPT_ELEM_WAIT =
            "CREATE TABLE " + SCRIPT_ELEM_WAIT_TABLE+ "(" +
                    SCRIPT_ELEM_WAIT_ID + " INTEGER PRIMARY KEY," +
                    SCRIPT_ELEM_WAIT_TIME + " INTEGER NOT NULL," +
                    " CONSTRAINT " + SCRIPT_ELEM_WAIT_FOREIGN_KEY_CONSTRAINT +" "+
                    SCRIPT_ELEM_WAIT_ID + " REFERENCES " + SCRIPT_TABLE + "(" + SCRIPT_ID + "))";
    */
    public static final String SCRIPT_ELEM_TABLE = "SCRIPTELEM";
    public static final String SCRIPT_ELEM_ID = "ID";
    public static final String SCRIPT_ELEM_POSITION = "position";
    public static final String SCRIPT_ELEM_COLOR = "color1";
    public static final String SCRIPT_ELEM_COLOR2 = "color2";
    public static final String SCRIPT_ELEM_TIME = "time";
    public static final String SCRIPT_ELEM_STEPS = "steps";
    public static final String SCRIPT_ELEM_INTERPOLATE_FOREIGN_KEY_CONSTRAINT = "ScriptElem_forgein_key_constraint";

    public static final String SQL_CREATE_SCRIPT_ELEM_INTERPOLATE =
            "CREATE TABLE " + SCRIPT_ELEM_TABLE + "(" +
                    SCRIPT_ELEM_ID + " INTEGER NOT NULL REFERENCES "+SCRIPT_TABLE+"(" + SCRIPT_ID + ")," +
                    SCRIPT_ELEM_POSITION + " INTEGER NOT NULL," +
                    SCRIPT_ELEM_COLOR+" INTEGER," +
                    SCRIPT_ELEM_COLOR2+" INTEGER," +
                    SCRIPT_ELEM_TIME+" INTEGER," +
                    SCRIPT_ELEM_STEPS+" INTEGER," +
                    "PRIMARY KEY("+SCRIPT_ELEM_ID+","+SCRIPT_ELEM_POSITION+")" +
                    ")";


    public ScriptDbHelper(Context context){
        super(context, DB_NAME, null, DB_Version);
        Log.d(LOG_TAG, "DbHelper created:"+getDatabaseName());
        onCreate(getWritableDatabase());
    }

    @Override
    public void onCreate(SQLiteDatabase db){

        try{
            Log.d(LOG_TAG, "Try to create table:"+SQL_CREATE_SCRIPT);
            db.execSQL(SQL_CREATE_SCRIPT);
        }
        catch(Exception ex){
            Log.e(LOG_TAG, "SQL-error on creating Database" + ex.getMessage());
        }
/*
        try{
            Log.d(LOG_TAG, "Try to create table:"+SQL_CREATE_SCRIPT_ELEM);
            db.execSQL(SQL_CREATE_SCRIPT_ELEM);
        }
        catch(Exception ex){
            Log.e(LOG_TAG, "SQL-error on creating Database" + ex.getMessage());
        }

        try{
            Log.d(LOG_TAG, "Try to create table:"+SQL_CREATE_SCRIPT_ELEM_SET);
            db.execSQL(SQL_CREATE_SCRIPT_ELEM_SET);
        }
        catch(Exception ex){
            Log.e(LOG_TAG, "SQL-error on creating Database" + ex.getMessage());
        }

        try{
            Log.d(LOG_TAG, "Try to create table:"+SQL_CREATE_SCRIPT_ELEM_WAIT);
            db.execSQL(SQL_CREATE_SCRIPT_ELEM_WAIT);
        }
        catch(Exception ex){
            Log.e(LOG_TAG, "SQL-error on creating Database" + ex.getMessage());
        }
*/
        try{
            Log.d(LOG_TAG, "Try to create table:"+SQL_CREATE_SCRIPT_ELEM_INTERPOLATE);
            db.execSQL(SQL_CREATE_SCRIPT_ELEM_INTERPOLATE);
        }
        catch(Exception ex){
            Log.e(LOG_TAG, "SQL-error on creating Database" + ex.getMessage());
        }


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){

    }
}
