/*
 * This file is part of raspberry-led
 *
 * Copyright (C) 2017 BavarianCoders
 *
 * raspberry-led is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.bavariancoders.raspberryled.storage;

import org.bavariancoders.raspberryled.model.Container;

/**
 * Created by BavarianCoders on 16.03.17
 */

public interface DataStorage<T> {
    void loadAll (Container<T> container) throws DataStorageException;
    T load (Container<T> container, int id) throws DataStorageException;
    void saveAll(Container<T> container) throws DataStorageException;
    void save (T saveT) throws DataStorageException;
    void modify (T newS) throws DataStorageException;
    void add (T newS) throws DataStorageException;
    void delete (T delT) throws DataStorageException;
    void close();
}
