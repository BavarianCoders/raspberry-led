# Raspberry LED
## What is it? ##
This is the Android App where you can control your LED-stripe connected to the GPIOs of your Raspberry Pi
over your local network.

### Features ###
* add new devices (Raspberry Pis) on the basis of IP address and port
* configure default LED color foreach device in the list
* change the LED color from a ColorWheel
* program your own light effects for example (fading or flash lights)
* turn all LEDs in your house on/off at once with the last selected color

### Important to know ###
To get this working you definitely need a **LED controller** which is connected to the Raspberry Pi's 
GPIO pins. Sadly no one offers a complete setup for sale but there is a awesome [guide](http://dordnung.de/raspberrypi-ledstrip/) 
from David Ordnung where he explains in detail how to build a LED controller on a breadboard. You
also need to run the **Server.py** python script on your Pi which you can get [here](https://bitbucket.org/BavarianCoders/ledstripeserver).
If your Pi runs LibreElec, you can install our [addon](https://bitbucket.org/BavarianCoders/service.bavariancoders.ledstripeserver/src)

### Building ###
Clone this repository and execute

* `gradle assembleFossRelease` for building the app without google dependencies or
* `gradle assembleGoogleRelease` for building with google dependencies

You can also build debug versions of the app. Just execute

* `gradle assembleFossDebug` or
* `gradle assembleGoogleDebug`

Please notice that you need a `KeyAlias`, `keyPassword`, `storeFile` and `storePassword` to sign
your build. Otherwise the build won't be signed. 

Obtain these values and modify this example with your values and put them in `gradle.properties` 
file in root dir of this project:

```
raspberryLEDSignStoreFile=/path/to/StoreFile
raspberryLEDSignStorePassword=yourStorePassword
raspberryLEDSignKeyAlias=yourKeysAlias
raspberryLEDSignKeyPassword=yourKeyPassword
```

### Screenshots ###

.                                                                                                                 | .
:----------------------------------------------------------------------------------------------------------------:|:---------------------------------------------------------------------------------------------------------------:
![photo5411388575548811185.jpg](https://bitbucket.org/repo/nqjAbE/images/1264777803-photo5411388575548811185.jpg) | ![photo5411388575548811185.jpg](https://bitbucket.org/repo/nqjAbE/images/1264777803-photo5411388575548811185.jpg)
![photo5411388575548811187.jpg](https://bitbucket.org/repo/nqjAbE/images/3612380284-photo5411388575548811187.jpg) | ![photo5411388575548811189.jpg](https://bitbucket.org/repo/nqjAbE/images/3946298425-photo5411388575548811189.jpg)
![photo5411388575548811195.jpg](https://bitbucket.org/repo/nqjAbE/images/1169268414-photo5411388575548811195.jpg) | 